
Determination and optimal combination of uncorrelated signal regions from LHC searches for new physics with TACO.
==================================================================

This repo stores the codes used for the paper ****Strength in numbers: optimal and scalable combination of LHC new-physics searches**** ( 	arXiv:2209.00025).

TACO (Testing Analyses' COrrelations) follows a probabilistic apporach to identify pairs of analyses that can safely be considered uncorrelated in global Beyond Standard Model reinterpretation fits.


**Running TACO**




python -f inputDirectory -o outputDirectory -c NumberOfCopies -t cut-off -k chunkSize
