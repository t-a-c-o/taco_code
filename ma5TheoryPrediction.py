"""
.. module:: ma5TheoryPrediction
   :synopsis: a facade to wrap ma5 output into something we can feed into
              SModelS' theoryPredictionCombiner

"""

fb, TeV = 1., 1. # as a fallback
try:
    from smodels.tools.physicsUnits import fb, TeV
except ImportError as e:
    pass

class MA5TheoryPrediction(object):
    """ the class that encapsulates MA5 output
    """

    class MA5CrossSection:
        def __init__ ( self, value, sqrts = 13*TeV ):
            """ production cross section
            :param value: the value of the production cross section
            :param sqrts: center of mass energy
            """
            self.value = value
            self.sqrts = sqrts

    class MA5DataSet:
        def __init__ ( self, srname ):
            """ param srname: the name of the signal region
                              ("dataset" is SModelS' name for a signal region) """
            self.srname = srname

        def getID( self ):
            return self.srname

    def __init__(self):
        """ constructor. not yet clear what we need. """
        self.xsection = MA5CrossSection( 1.*fb ) # the production cross section
        self.dataset = MA5DataSet( "srname" )
        self.analysisId_ = "unknown"
        # btw, we might wish to cache the computations, e.g.
        # self.cachedLlhds = {False: {}, True: {}, "posteriori": {}}

    def analysisId(self):
        """ return the analysis id, e.g. "atlas_susy_2018_02" """
        return self.analysisId_

    def dataType(self): # a technicality
        return "efficiencyMap"

    def getRValue ( self, expected = False ):
        """ the r-value is defined as the predicted cross section divided
            by the upper limit on the predicted cross section """
        # i guess that's ok the way it is
        return self.getUpperLimit ( expected ) / self.xsection.value
        
    def getUpperLimit ( self, expected = False ):
        # code here that retrieves the upper limit, expected or observed

    def likelihood( self, mu=1., expected=False, nll=False ):
        """ compute the likelihood for the signal strength mu
        :param mu: signal strength
        :param expected: if true, compute expected likelihood, else observed.
                         if "posteriori", compute a posteriori expected likelihood
                         (FIXME do we need this?)
        :param nll: if True, return the negative log likelihood instead of the
                    likelihood
        """
        # btw we might wish to cache the computations, e.g.
        # if mu in self.cachedLlhds[expected]:
        #     return the right value

    def muhat ( self, expected = False, allowNegativeSignals = True ):
        """ get the value of mu for which the likelihood is maximal.
            this is only used for initialisation of the optimizer, so can 
            be skipped in the first version of the code.
        :param expected: get muhat for the expected likelihood, not observed.
        :param allowNegativeSignals: if true, allow negative values for mu
        """
        return None

    def sigma_mu ( self, expected = False, allowNegativeSignals = True ):
        """ get an estimate for the standard deviation of mu around mu_hat
            this is only used for initialisation of the optimizer, so can 
            be skipped in the first version, it's even less important than muhat
        :param expected: get muhat for the expected likelihood, not observed.
        :param allowNegativeSignals: if true, allow negative values for mu
        """
        return None


if __name__ == "__main__":
    from smodels.tools.theoryPredictionsCombiner import TheoryPredictionsCombiner
    combiner = TheoryPredictionsCombiner ( predictions )
    print ( "ul", combiner.ul ( expected=False ) )
    print ( "expected ul", combiner.ul ( expected=True ) )
    print ( "r-value", combiner.getRValue ( ) )
