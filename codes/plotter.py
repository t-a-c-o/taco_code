#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 27 19:05:27 2019

@author: gonzales
"""

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import matplotlib
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages


    
    
def create_translation_table(SR_tag,analysis):

    SR_tag_dict={}
    j=0
    for sr in SR_tag:
        j=j+1
        SR_tag_dict[sr]='SR'+str(j)

        
    dframe=pd.DataFrame()
    dframe['tag']=SR_tag_dict.values()
    dframe['name']=SR_tag_dict.keys()
    fig, ax =plt.subplots(figsize=(12,4))
    ax.axis('tight')
    ax.axis('off')
    the_table = ax.table(cellText=dframe.values,colLabels=dframe.columns,loc='center')

    #https://stackoverflow.com/questions/4042192/reduce-left-and-right-margins-in-matplotlib-plot
    pp = PdfPages("plots_test/"+str(analysis)+".pdf")
    pp.savefig(fig, bbox_inches='tight')
    pp.close()
    ax.cla()
    fig.clf()
        
    return SR_tag_dict
        
        
        

def Ploter(df,list_1,list_2,an,an_2,SR_tag_1,SR_tag_2):
        
    df_an=df.loc[list_2,list_1]
    
    
    
    if  all([x == 1 for x in list(df_an.nunique())])  == 1:
        print(an)
        if df_an.iat[0,0]==0:
            myColors = ((0.8, 0.0, 0.0, 1.0),(0.0, 0.8, 0.0, 1.0))
        elif df_an.iat[0,0]==1:
            myColors = ((0.8, 0.0, 0.0, 1.0),(0.0, 0.8, 0.0, 1.0))
            
    else:      
         
        myColors = ((0.0, 0.8, 0.0, 1.0),(0.8, 0.0, 0.0, 1.0))
    cmap = LinearSegmentedColormap.from_list('Custom', myColors, len(myColors))
    
    
    
    if an==an_2:
        mask = np.triu(np.ones_like(df_an, dtype=np.bool))
        #mask = np.zeros_like(df_an, dtype=np.bool)
        #mask[np.triu_indices_from(mask)] = True
            # Want diagonal elements as well
        mask[np.diag_indices_from(mask)] = False
            
        plot=sns.heatmap(df_an, annot=False,linewidths=.5,cmap=cmap,mask=mask,linecolor='white',cbar_kws={'orientation': 'vertical'})
            
    else:
        plot=sns.heatmap(df_an, annot=False,linewidths=.5,cmap=cmap,linecolor='white',cbar_kws={'orientation': 'vertical'})
        
    colorbar = plot.collections[0].colorbar
    colorbar.remove()

    
    
    font = {'size'   : 10}

    matplotlib.rc('font', **font)

    xtick_list=[]
    for j in range(len(SR_tag_1)):
        xtick_list.append(j+.5)
        
    ytick_list=[]
    for j in range(len(SR_tag_2)):
        ytick_list.append(j+.5)        
        
    

    plt.xticks((xtick_list),(SR_tag_1),rotation = 90,fontsize=9)
    plt.yticks((ytick_list),(SR_tag_2),rotation = 0,fontsize=9)

    plt.xlabel(an,fontsize=10)
    plt.ylabel(an_2,fontsize=10)
    

     
    matplotlib.rc('xtick', labelsize=7) 
    matplotlib.rc('ytick', labelsize=7)
    plt.tight_layout(rect=[.01,.01,.99,.99])
    

    plot.figure.savefig("plots_test/corr_plot_"+an+"_"+an_2+".png",bbox_inches=None)
        
    plt.close()
    
    return







df = pd.read_csv('../TACOing/ind_matrix_8TeV_1_largerthan500_1000.txt', index_col='index')



####Separate analysis names and sr names
all_srs=list(df.columns)
all_analyses={}
all_analyses_names_in_dataframe={}
for sr in all_srs:

    analysis_name=sr.split('-')[0]
    sr_name='-'.join(sr.split('-')[1:])
    
    if analysis_name not in all_analyses.keys():
        all_analyses[analysis_name]=[sr_name]
        all_analyses_names_in_dataframe[analysis_name]=[sr]
    else:
        all_analyses.get(analysis_name).append(sr_name)
        all_analyses_names_in_dataframe.get(analysis_name).append(sr)


## Giving tags to the SRs
anlysis_sr_tags_dict={}
for analysis in all_analyses.keys():
    SR_tag=all_analyses.get(analysis)
    SR_tag_dict=create_translation_table(SR_tag,analysis)
    anlysis_sr_tags_dict[analysis]=SR_tag_dict
    

    

##Plotting
for an in all_analyses.keys():
    for an_2 in all_analyses.keys():
      
        list_1=all_analyses_names_in_dataframe.get(an)
        list_2=all_analyses_names_in_dataframe.get(an_2)
        SR_tag_1=anlysis_sr_tags_dict.get(an).values()
        SR_tag_2=anlysis_sr_tags_dict.get(an_2).values()
        
        
        Ploter(df,list_1,list_2,an,an_2,SR_tag_1,SR_tag_2)
        print(an+'_'+an_2+' done')

    




        

