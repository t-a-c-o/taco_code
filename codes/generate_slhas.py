#!/usr/bin/env python

############################################################################################

# 'Determination of Independent Signal Regions in LHC Searches for New Physics'             #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger and S. Williamson.             #
############################################################################################

# from __future__ import print_function

#~~~~~~~
#script can be separated up into stand alone bits (denoted by --%-- sections) depending on which information has been produced/files are installed for the user.
#~~~~~~


import os
import pandas as pd
import math


def pick_templates(topologies):

    for topo in topologies:
        print(topo)
        os.system('cp all_templates/'+topo+'.template current_templates/')
    return
        
        
def add_missing_blocks(topologies):

    for topo in topologies:
        template_read=open('current_templates/'+topo+'.template').read()
        
        if 'BLOCK YU' not in template_read:
            print(topo)
            edited_template=template_read.replace('BLOCK MSOFT','BLOCK YU Q=  4.67034192E+02  # The Yukawa couplings\n  3  3     8.92844550E-01   # y_t(Q) DRbar\n#\nBLOCK YD Q=  4.67034192E+02  # The Yukawa couplings\n  3  3     1.38840206E-01   # y_b(Q) DRbar\n#\nBLOCK YE Q=  4.67034192E+02  # The Yukawa couplings\n  3  3     1.00890810E-01   # y_tau(Q) DRbar\n#\nBLOCK MSOFT')
    
        
            new_template=open('current_templates_edited/'+topo+'.template','w')
            new_template.write(edited_template)
            new_template.close()
        else:
            os.system('cp current_templates/'+topo+'.template current_templates_edited/')


def add_SM_zero_mass(topologies):


    for topo in topologies:
        template_read=open('current_templates_edited/'+topo+'.template').read()
        
        print(topo)
        edited_template=template_read.replace('# PDG code           mass       particle','# PDG code           mass       particle\n         1     0.00000000E+00  # Fd_1\n         3     0.00000000E+00  # Fd_2\n         5     4.18000000E+00  # Fd_3\n         2     0.00000000E+00  # Fu_1\n         4     0.00000000E+00  # Fu_2\n         6     1.73500000E+02  # Fu_3\n        11     0.00000000E+00  # Fe_1\n        13     0.00000000E+00  # Fe_2\n        15     0.00000000E+00  # Fe_3\n        12     0.00000000E+00  # Fv_1\n        14     0.00000000E+00  # Fv_2\n        16     0.00000000E+00  # Fv_3')
    
        
        new_template=open('current_templates_edited/'+topo+'.template','w')
        new_template.write(edited_template)
        new_template.close()

    return

def truncate(number):
    "truncate float to 3 decimal places"
    
    if number<1 and number>1e-90:
    
        provisional_number=number
        order=0
    
        while provisional_number<1:
        
            provisional_number=provisional_number*(10)
            order=order+1
            
        
        factor=10**1
        truncated=(math.trunc(provisional_number * factor) / (factor*10**(order)))
    else:
        factor=10**1
        truncated=math.trunc(number * factor) / factor
    return truncated


def generate_slhas(topologies):


    for topo in topologies:
        print(topo)
    
        template=open('current_templates_edited/'+topo+'.template').read()
        points_frame=pd.read_csv('../convex_hull/data_frames_ATLAS-SUSY-2013-21/'+topo+'.txt')
        
        os.system('mkdir slhas_ATLAS-SUSY-2013-21/'+topo)
        if points_frame.shape[1]==2:
        
            for row in range(points_frame.shape[0]):
                m_0=str(int(points_frame['m_1'][row]))
                m_1=str(int(points_frame ['m_2'][row]))
                new_slha_name=topo+'_m0_'+m_0+'_m1_'+m_1+'.slha'
                
                new_slha=open('slhas_ATLAS-SUSY-2013-21/'+topo+'/'+new_slha_name,'w')
                
                new_slha_data=template.replace('M0',' '+m_0+' ')
                new_slha_data=new_slha_data.replace('m0',' '+m_0+' ')
                new_slha_data=new_slha_data.replace('M1',' '+m_1+' ')
                new_slha_data=new_slha_data.replace('m1',' '+m_1+' ')
                
                new_slha.write(new_slha_data)
                new_slha.close()
                
        
        elif points_frame.shape[1]==3:
            print(points_frame.shape[0])
            for row in range(points_frame.shape[0]):
                m_0=str(int(points_frame['m_1'][row]))
                m_1=str(int(points_frame['m_2'][row]))
                m_2=str(int(points_frame['m_3'][row]))
                new_slha_name=topo+'_m0_'+m_0+'_m1_'+m_1+'_m2_'+m_2+'.slha'
                
                new_slha=open('slhas_ATLAS-SUSY-2013-21/'+topo+'/'+new_slha_name,'w')
                
                new_slha_data=template.replace('M0',' '+m_0+' ')
                new_slha_data=new_slha_data.replace('m0',' '+m_0+' ')
                new_slha_data=new_slha_data.replace('M1',' '+m_1+' ')
                new_slha_data=new_slha_data.replace('m1',' '+m_1+' ')
                new_slha_data=new_slha_data.replace('M2',' '+m_2+' ')
                new_slha_data=new_slha_data.replace('m2',' '+m_2+' ')
                
                new_slha.write(new_slha_data)
                new_slha.close()
        
    

    return

    





topologies=['T2cc', 'T2bb', 'T2bbWW', 'T2bbWWoff']


#pick_templates(topologies)
add_missing_blocks(topologies)
add_SM_zero_mass(topologies)
generate_slhas(topologies)
