#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 14:34:04 2020

@author: gonzales
"""

import pandas as pd
import os

run_name='TACO_8TeV_done'
dir_scan='/mnt/project_mnt/teo_fs/hreyes/TACO/event_sampling'+'/'+run_name

topos=os.listdir(dir_scan)

all_events_list=[]
for topo in topos:

  #  if topo=='TChiWZoff':
  #      continue

    if topo=='.DS_Store':
        continue
        
    if topo=='all_events.txt':
        continue

    dir_corr=dir_scan+'/'+topo+'/correlation_output/'
    
    corr_data=os.listdir(dir_corr)
    
    corr_frames=[]
    
    temp_dir=dir_corr+'temp_dir/'
    os.system('mkdir '+temp_dir)
    
    name_concatenated_analysis=dir_corr+'concatenated/'
    os.system('mkdir '+name_concatenated_analysis)
    
    for corr in corr_data:
    
        if corr=='.DS_Store':
            continue
        if corr=='concatenated':
            continue
        if corr=='temp_dir':
            continue
        print(corr)
        corr_read=open(dir_corr+corr).read()
        corr_open=open(dir_corr+corr)
        j=0
        
        
      #  corr_read=corr_read.replace(' atlas','&atlas')
      #  corr_read=corr_read.replace(' &atlas','&atlas')
      #  corr_read=corr_read.replace(' cms','&cms')
      #  corr_read=corr_read.replace(' &cms','&cms')
      #  corr_read=corr_read.replace('#&atlas','#  atlas')
      #  corr_read=corr_read.replace('#&cms','#  cms')
        #corr_read=corr_read.replace(',','-')
        #corr_read=corr_read.replace(' ','')
        corr_read=corr_read.replace('\n  ','\n')
        corr_read=corr_read.replace('  ',' ')
        corr_read=corr_read.replace('0 ','0&')
        corr_read=corr_read.replace('1 ','1&')

        #corr_read=corr_read.replace('0&\n','0\n')
        #corr_read=corr_read.replace('1&\n','1\n')
        
        corr_read_splitted=corr_read.split('# ')
        
        
        new_corr_read_splitted=[]
        for splitting in corr_read_splitted:
            # print(splitting)
            split_lines=splitting.split('\n')
            if len(split_lines)<2:
                continue
           # print(len(split_lines))
            new_column_row=split_lines[0].replace('&',' ')
            new_column_row=new_column_row.replace(' atlas','&atlas')
            new_column_row=new_column_row.replace(' cms','&cms')
            new_column_row=new_column_row.replace(',','-')
            new_column_row=new_column_row.replace(' ','')
        #    print(new_column_row)
    
            split_lines[0]=new_column_row
            split_lines.pop(1)
            new_splitting='\n'.join(split_lines)
            
            new_corr_read_splitted.append(new_splitting)
            
            
        
            
        
        
        
        list_of_temp_frames=[]
        
        print(len(new_corr_read_splitted))
        for j in range(0,len(new_corr_read_splitted)):
            temp_file_name=temp_dir+corr+'_'+str(j)+'.txt'
            temp_file=open(temp_file_name,'w')
            
       
            
            
            
            temp_file.write(new_corr_read_splitted[j])
            temp_file.close()
            
            temp_frame=pd.read_csv(temp_file_name,sep='&')
            print(temp_frame.columns)
            print(temp_frame.head())
            temp_frame=temp_frame.fillna(0)
            temp_frame=temp_frame.astype(int)
           # col_name_temp_frame=temp_frame.columns[0]
           # if 'cms' in col_name_temp_frame:
           #     print('found')
           #     continue
            list_of_temp_frames.append(temp_frame)
            
        concatenated_frames=pd.concat(list_of_temp_frames,axis=1)
        
        
        concatenated_frames.to_csv(name_concatenated_analysis+corr,index=False)
        
        corr_frames.append(concatenated_frames)
        
        
    all_topo_events=pd.concat(corr_frames,axis=0)
    all_topo_events=all_topo_events.fillna(0)
    all_topo_events=all_topo_events.astype(int)
    
  
    
    
    all_topo_events_name=dir_scan+'/'+topo+'/concatenated_events.txt'
    
    
    all_topo_events.to_csv(all_topo_events_name,index=False)
    
    
    all_events_list.append(all_topo_events)
    
    
all_events_frame=pd.concat(all_events_list,axis=0)

all_events_frame.to_csv(dir_scan+'/'+run_name+'_all_events.txt',index=False)



all_events_frame=pd.read_csv(dir_scan+'/'+run_name+'_all_events.txt')

print(all_events_frame.head())
print(all_events_frame.shape)


all_events_frame=all_events_frame.fillna(0)
        
print(all_events_frame.sum(axis = 0, skipna = True))
print(all_events_frame.head())
print(all_events_frame.shape)
    
        


dataframe=pd.read_csv('frame_events_8TeV_1_largertahn500.txt')
print(dataframe.shape[0])
print(dataframe.shape[1])

###Dropping rows with all zeros for efficiency.

dataframe_droppedzeros=dataframe.loc[(dataframe!=0).any(axis=1)]
print(dataframe_droppedzeros.shape[0])
dataframe.to_csv('frame_events_8TeV_1_largertahn500_droppedzeros.txt')

