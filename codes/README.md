The scan pipeline goes like this:

1) TACO_convex_huller.py : Creates convex hulls for each topology.
2) TACO_create_data_tables.py: Creates the mass samples for each topology.
3) generate_slhas.py: Generates the slhas corresponding to the mass samples for each topology, by editing slha templates.
4)prepare_scan.py: Creates the directories in which the inputs and outputs of the scan will go.
5) The processes are written manually for each topology in the corresponding proc.txt
6) run_events.py: Produces the events. Madgraph and Madanalysis required. It also assumes that Madanalysis has been edited to print one-on-one events.

The independency matrix production goes like this:

1) concat_topos.py : Concatenates the one-on-one event files in each topology.

2)TACO.py: TACO procedure.

