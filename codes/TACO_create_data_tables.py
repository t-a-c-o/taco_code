#!/usr/bin/env python

############################################################################################
# Script written to produce convex mass hulls, as a continuation of the LH2019 project       #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'             #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger and S. Williamson.             #
############################################################################################

# from __future__ import print_function

#~~~~~~~
#script can be separated up into stand alone bits (denoted by --%-- sections) depending on which information has been produced/files are installed for the user.
#~~~~~~

import re
from numpy import *
import os
import pandas as pd
import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup
import requests
import shutil
import numpy as np
from scipy.spatial import ConvexHull, convex_hull_plot_2d
import matplotlib.pyplot as plt
import pickle
import random as rand

with open( "ATLAS-SUSY-2013-21Dictionary.pcl", "rb" ) as TheSamePickle:
    MassiveDictionary = pickle.load(TheSamePickle)


print(MassiveDictionary.keys())
Hulls={}

# This function (ConvexHullPlot2d_Query) is to aid visual understanding of the code, and can be commented.
#-------#


def ConvexHullPlot2d_Query (TxKey, SortedMasses, p):
    """2D plot of the convex hull for a topology involving 2 body decays. SortedMasses are those that lie in the convex hull; Hulls[TxKey] is the hull for a certain topology; p is a query point. A boundary is drawn between the outermost points of the convex hull, and the individual points plotted on the graph. """
    if(len(SortedMasses[0])==2):
        plt.plot(SortedMasses[:,0], SortedMasses[:,1], 'o',markersize=4.5)
        for simplex in Hulls[TxKey].simplices:
            plt.plot(SortedMasses[simplex, 0], SortedMasses[simplex, 1], 'k-',)
            plt.title(TxKey)
            plt.plot(p[:,0], p[:,1], 's',markersize=4.5)
        plt.show()
#-------#

#Mapping: This should be edited to check a large number of points for each topology.
def QueryHull(OldHull, MassPoints, p):
    """Function to query whether a new mass-point coordinate, p, lies within the convex hull of the given data, OldHull. """
    NewPoints = np.append(MassPoints, p, axis=0)
    NewHull = ConvexHull(NewPoints,qhull_options='QJ')
    if list(OldHull.vertices) == list(NewHull.vertices):
        return True
    else:
        return False


for TxKey, MassValues in MassiveDictionary.items():
    SortedMasses = np.array(sorted(MassValues)) # Mass order the tuples
    print(TxKey)
    print(len(SortedMasses[0]))
    '''
    if TxKey=='TChipChimSlepSlep':
        print('skipping TChipChimSlepSlep')
        continue
    '''
    #already_produced=os.listdir('data_frames')
    #if TxKey+'.txt' in already_produced:
    #    continue
    #-----------------------#

    #These next lines generate one point to check in each topology. We want to edit here to generate an array of data points which we query in the convex hull, Hulls[TxKey], with the good ones (QueryHull->True) being saved for data point generation.

    Hulls[TxKey] = ConvexHull(points=SortedMasses, qhull_options='QJ') #this is the original/old hull.
   # if(len(SortedMasses[0])==2): # Check whether the hull is 2- or 3- dimensional and generate one random query point within the range below.
   #     p = np.random.rand(1, 2)*400 # 1 random 2D query point
   # else: p = np.random.rand(1, 3)*400 # 1 random 3D query point

    #-----------------------#
    n_masses=len(SortedMasses[0])
    j=0
    data_list=[]
    
    for m in range(n_masses):
        print(m)
        print('max')
        print(max(SortedMasses[:,m]))
        print('min')
        print(min(SortedMasses[:,m]))
    print('xxxxxxx')
    
            
    while j<500:
        
       # if j%100==0:
       #     print(float(j/1000))
        point_list=[]
        
        if TxKey=='TChiChipmStauStau' or TxKey=='TChipChimSlepSlep':
            p0=rand.uniform(min(SortedMasses[:,m]),max(SortedMasses[:,0]))
            p2=rand.uniform(min(SortedMasses[:,m]),max(SortedMasses[:,2]))
            p1=.5*p0+.5*p2
                
            point_list=[p0,p1,p2]
            
        if TxKey=='T5tctc':
            p0=rand.uniform(min(SortedMasses[:,m]),max(SortedMasses[:,0]))
            p2=rand.uniform(min(SortedMasses[:,m]),max(SortedMasses[:,2]))
            p1=p2+20
                
            point_list=[p0,p1,p2]
            
            
        
        else:
            for m in range(n_masses):
 
                p = rand.uniform(min(SortedMasses[:,m]),max(SortedMasses[:,m]))
                point_list.append(p)
        query=QueryHull(Hulls[TxKey],SortedMasses,[point_list])
        #print("Is point p", p, " in hull? ", query)
        
        if query==True:
            data_list.append(point_list)
            if TxKey=='TChiChipmStauStau':
                print('true')
            j=j+1
         
       
    
    if n_masses==2:
        column_names=['m_1','m_2']
    elif n_masses==3:
        column_names=['m_1','m_2','m_3']
 
    dataframe=pd.DataFrame.from_records(data_list,columns=column_names)
    dataframe.to_csv('data_frames_ATLAS-SUSY-2013-21/'+str(TxKey)+'.txt',index=False)
   
   
   
   # if(len(SortedMasses[0])==2):
   #     ConvexHullPlot2d_Query(TxKey, SortedMasses, p)

    #-----------------------#
