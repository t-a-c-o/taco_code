#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
import copy
import numpy as np
import pandas as pd



class JoinCorrelations():

    def __init__ (self, df_list:list) -> None:
        self.__primed = self.__prime_class(df_list)
        self.labels = self.__all_labels(df_list)
        self.combined_df = self.combine_correlations(df_list)
        
    def __prime_class(self, all_df):
        primed = all([self.__check_shape(item) for item in all_df])
        if not primed:
            print('!!! NON SQUARE MATRIX !!!')
            print('Check dimensions of data provided')
        return primed

    @staticmethod
    def __check_shape(vals: np.ndarray) -> bool:
        return vals.shape[0] == vals.shape[1]

    @staticmethod
    def __all_labels(all_df) -> list:
        return [item for df in all_df for item in [*df]]

    def __combine_values(self, all_df) -> np.ndarray:
        if not self.__primed:
            return np.array([])
        dims = np.array([0] + [item.shape[0] for item in all_df])
        step = np.cumsum(dims)
        dim = np.sum(dims)
        new_matrix = np.zeros((dim, dim))
        for i, item in enumerate(all_df):
            #print(step[i], step[i+1])
            new_matrix[step[i]:step[i+1], step[i]:step[i+1]] = item.to_numpy()
        return new_matrix
    
    def apply_acceptence(self, dfaccep:pd.DataFrame, val:float=1.0)-> None:

        labs = [*dfaccep]
        points = list(np.argwhere((dfaccep.to_numpy() == -1)))
        all_values = np.copy(self.combined_df.to_numpy())
        if points:
            for p in points:
                sr0 = labs[p[0]]
                if sr0 in self.labels:
                    sr1 = labs[p[1]]
                    if sr1 in self.labels:
                        idx = (self.labels.index(sr0), self.labels.index(sr1))
                        all_values[idx] = val

            self.combined_df = pd.DataFrame(data=all_values,
                                            index=self.labels,
                                            columns=self.labels)


    def combine_correlations(self, df_list) -> pd.DataFrame:
        if not self.__primed:
            return pd.DataFrame()
        vals = self.__combine_values(df_list)
        return pd.DataFrame(data=vals, 
                            index=self.labels, 
                            columns=self.labels)

    def save_new_correlation(self, f_path: str) -> None:
        self.combined_df.to_csv(f_path)

if __name__ == '__main__':
    #cor_loc_13 = 'Data/S_13TeV/correlations/corr_matrix_TACO_13TeV_run1_largerthan500_2000copies.txt'
    #cor_loc_8 = 'Data/S_8TeV/correlations/correlation_matrix_largerthan500_1000copies.txt'

    cor_loc_13_atlas = 'Full_SR_Ranking/Data/13TeV_2/corr_matrix_TACO_13TeV_atlas_3000copies.txt'
    cor_loc_13_cms = 'Full_SR_Ranking/Data/13TeV_2/corr_matrix_TACO_13TeV_cms_3000copies.txt'
    cor_loc_8 = 'Full_SR_Ranking/Data/8TeV_2/correlation_matrix_8TeV_2.txt'

    acceptence13_atlas = 'Full_SR_Ranking/Data/13TeV_2/acceptance_matrix_13TeV_3456789_atlas.txt'
    acceptence13_cms = 'Full_SR_Ranking/Data/13TeV_2/acceptance_matrix_13TeV_3456789_cms.txt'
    acceptence8 = 'Full_SR_Ranking/Data/8TeV_2/acceptance_matrix_8TeV.txt'

    ### correlations
    dfa = pd.read_csv(cor_loc_13_atlas , index_col=0)
    dfc = pd.read_csv(cor_loc_13_cms , index_col=0)
    df8 = pd.read_csv(cor_loc_8 , index_col=0)
    ### acceptence
    dfacc8 = pd.read_csv(acceptence8 , index_col=0)
    dfacc13_ats = pd.read_csv(acceptence13_atlas , index_col=0)
    dfacc13_cms = pd.read_csv(acceptence13_cms , index_col=0)
    #initiate join corelations class
    jcorr = JoinCorrelations([dfa, dfc, df8])
    df = jcorr.combined_df
    jcorr.apply_acceptence(dfacc8)
    jcorr.apply_acceptence(dfacc13_ats)
    jcorr.apply_acceptence(dfacc13_cms)


    ######
    points = np.argwhere((dfacc13_cms.to_numpy() == -1))
    labs = [*dfacc13_cms]
    p = points[0]
    print(labs[p[0]], labs[p[1]])
    print(dfacc13_cms.loc[labs[p[0]]][labs[p[1]]])
    print(df.loc[labs[p[0]]][labs[p[1]]])
    print(jcorr.combined_df.loc[labs[p[0]]][labs[p[1]]])
    
    jcorr.save_new_correlation('Full_SR_Ranking/Data/S_8_13Tev/correlations/combined_correlation_matrix1.txt')