#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
from __future__ import print_function
from typing import Tuple
import copy
import scipy.stats
import numpy as np

from smodels.experiment.databaseObj import Database
from smodels.theory.theoryPrediction import theoryPredictionsFor
from smodels.theory.decomposer import decompose
from smodels.particlesLoader import BSMList
from smodels.share.models.SMparticles import SMList
from smodels.theory.model import Model
from smodels.tools.physicsUnits import GeV, fb
from smodels.theory import decomposer

class CombineSignalRegions():
    """
    Empty class docstring
    """
    def __init__ (self) -> None:

        self.exp_res = None

    def combine_results(self, database:Database, anas_and_sr : dict, debug=False ) -> None:
        """ combine the <anas_and_sr> results in <database>
            to a single result with a diagonal covariance matrix
        :param anas_and_sr: dictionary with analysis Ids as keys and lists of signal regions
            as values, e.g.: { "ATLAS-SUSY-2016-07": ['2j_Meff_1200', '2j_Meff_1600'] }
        :returns: a combined ExpResults
        """

        anaids = anas_and_sr.keys()
        exp_results = database.getExpResults( analysisIDs = anaids,
                                            dataTypes = [ "efficiencyMap" ] )
        datasets, datasetorder, covariance_matrix = [], [], []
        ana_ids = []
        n_datasets = 0
        for e_r in exp_results:
            ana_id = e_r.globalInfo.id
            ana_ids.append ( ana_id )
            srgns = anas_and_sr[ana_id]
            for s_r in srgns:
                d_s = e_r.getDataset ( s_r )
                if d_s is not None:
                    n_datasets+=1

        ctds=0
        for e_r in exp_results:
            ana_id = e_r.globalInfo.id
            #ana_ids.append ( ana_id )
            srgns = anas_and_sr[ana_id]
            for s_r in srgns:
                d_s = e_r.getDataset ( s_r )
                cov_row = [0.]*n_datasets
                if d_s is not None:
                    datasets.append ( d_s )
                    datasetorder.append ( d_s.dataInfo.dataId )
                    cov_row[ctds]= d_s.dataInfo.bgError**2
                covariance_matrix.append ( cov_row )
                ctds += 1

        if debug:
            print ( "[combine_results] cov_matrx", covariance_matrix )
            print ( "[combine_results] datasets", datasets )
            print ( "[combine_results] ana_ids", ana_ids )
        ## construct a fake result with these <n> datasets and and
        ## an nxn covariance matrix
        if exp_results:
            self.exp_res = copy.deepcopy ( exp_results[0] )
            self.exp_res.datasets = datasets
            self.exp_res.ana_ids = ana_ids
            self.exp_res.globalInfo.datasetOrder = datasetorder
            self.exp_res.globalInfo.covariance = covariance_matrix
        else:
            self.exp_res = None

    def single_result(self, database:Database, anas_and_sr : dict, num:int=0):

        datasetIDs = [item for _, sublist in anas_and_sr.items() for item in sublist]
        exp_results = database.getExpResults( analysisIDs =[*anas_and_sr], datasetIDs=datasetIDs, dataTypes = [ "efficiencyMap" ] )
        if exp_results:
            self.exp_res = copy.deepcopy ( exp_results[num] )
        else:
            self.exp_res = None

    def r_value(self, slhafile: str, anas_and_sr : dict,
                database:Database=None, combine:bool=True, verbose:bool=False)-> Tuple[float, float]:
        """calculate r value for combined signal regions"""

        if database is None:
            dbpath = "official"
            database = Database( dbpath )

        if combine:
            self.combine_results ( database, anas_and_sr, verbose )

        else:
            self.single_result(database, anas_and_sr)

        if self.exp_res is None:
            tpred = None
        else:
            model = Model(BSMparticles=BSMList, SMparticles=SMList )
            model.updateParticles(inputFile=slhafile )
            smstopos = decompose ( model )
            tpred = theoryPredictionsFor (self.exp_res, smstopos, combinedResults=combine,
                                          useBestDataset=False, marginalize=False )
        if tpred is None:
            robs = None
            rexp = None
        else:
            robs = tpred[0].getRValue ( expected=False )
            rexp = tpred[0].getRValue ( expected=True )


        if verbose:
            print ( "robs:", robs, "rexp:", rexp )
        return robs, rexp

    def chi_value(self, slhafile: str, anas_and_sr : dict, database:Database=None, 
                expected: bool=False, combine=True, verbose=False) -> Tuple[float, float]:
        """calculate p value for combined signal regions"""
        if database is None:
            dbpath = "official"
            database = Database( dbpath )

        if combine:
            self.combine_results ( database, anas_and_sr, verbose )

        else:
            self.single_result(database, anas_and_sr)

        model = Model(BSMparticles=BSMList, SMparticles=SMList)
        model.updateParticles(inputFile=slhafile)
        sigmacut = 0.005*fb
        mingap = 5.*GeV

        toplist = decomposer.decompose(model, sigmacut, doCompress=True,
                                       doInvisible=True, minmassgap=mingap)

        predictions = theoryPredictionsFor(self.exp_res, toplist, useBestDataset=False,
                                           combinedResults=combine, marginalize=False)
        #self.pred_obj = predictions
        if verbose:
            print ( "predictions", predictions )
            
        pval, chi2 = np.NaN, np.NaN

        likelihood, lmax = np.NaN, np.NaN

        if predictions is not None:
            pred = predictions[0]
            if pred is not None:
                pred.computeStatistics(expected=expected)
                #print(pred.dataType())
                #print(pred)
                #print ( "Lmax", pred.lmax, "l", pred.likelihood, "Lsm", pred.lsm )

                ## now here is the fun part, the theory prediction object "pred" knows 
                ## the likelihood of the model (pred.likelihood), the likelihood at 
                ## the signal strength that maximizes the likelihood (lmax), and the 
                ## likelihood of the signal strength = 0., aka the standard model (lsm)
                if expected:
                    likelihood = pred.elikelihood
                    lmax = pred.elmax 
                else:
                    likelihood = pred.likelihood
                    lmax = pred.lmax 
                if likelihood != 0.0 and lmax != 0.0:
                    chi2 = -2.0 * np.log ( likelihood / lmax )
    
                    if verbose:
                        print('dataSet: ', pred.dataset)
                        print ( "chi2 = ", chi2, "p = ", pval)                

        return np.array([chi2, likelihood, lmax])

    @staticmethod
    def chi_to_p(chi2, df=1):
        return scipy.stats.chi2.sf (chi2, df=df)

    @property
    def covariance(self) -> np.ndarray:
        """ return covarience """
        if self.exp_res is None:
            print('Experimental result not set!!!')
        else:
            return np.array(self.exp_res.globalInfo.covariance)
        return np.array([])

if __name__ == "__main__":
    # examplary combinations
    csr = CombineSignalRegions()
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750" ] } )
    print ( "now compute r value for single result", r )
    print ( "cov", csr.covariance )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750", "SR1_Njet2_Nb0_HT500_MHT500" ] } )
    print ( )
    print ( "now compute r value for two SRs", r )
    print ( "cov", csr.covariance )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750", "SR1_Njet2_Nb0_HT500_MHT500" ], "CMS-SUS-13-013": [ "SR22_HighPt" ] } )
    print ( )
    print ( "now compute r value, for 8 + 13", r )
    print ( "cov", csr.covariance )
