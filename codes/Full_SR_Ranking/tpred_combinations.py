#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
from __future__ import print_function
import copy
#import scipy.stats
import numpy as np

from scipy.stats.distributions import chi2

from smodels.experiment.databaseObj import Database
from smodels.theory.theoryPrediction import TheoryPredictionList, theoryPredictionsFor
from smodels.theory.decomposer import decompose
from smodels.particlesLoader import BSMList
from smodels.share.models.SMparticles import SMList
from smodels.theory.model import Model
from smodels.tools.physicsUnits import GeV, fb
from smodels.theory.topology import TopologyList
try:
    from smodels.tools.theoryPredictionsCombiner import TheoryPredictionsCombiner
except ModuleNotFoundError as e:
    from smodels.theory.theoryPrediction import TheoryPredictionsCombiner
    
from smodels.experiment.exceptions import SModelSExperimentError



class CombineSignalRegions():
    """
    Empty class docstring
    """
    def __init__ (self) -> None:

        self.tops = None

    @staticmethod
    def exp_results(database:Database, anas_and_sr : dict) -> list:
        """
        returns: list of ExpResult objects or the ExpResult 
        object if the list contains only one result 
        """
        datasetIDs = [item for _, sublist in anas_and_sr.items() for item in sublist]
        return database.getExpResults( analysisIDs =[*anas_and_sr],
                                       datasetIDs=datasetIDs, 
                                       dataTypes =["efficiencyMap"],
                                       useNonValidated=False, 
                                       onlyWithExpected=False)
        

    @staticmethod
    def set_model(slhafile:str) -> Model:
        """
        Returns an An instance of the SModelS Model class that 
        holds all the relevant information from the slha file. 
        The class contains the input file, the particles of the
        model and their production cross-sections.
        """
        model = Model(BSMparticles=BSMList, SMparticles=SMList)
        model.updateParticles(inputFile=slhafile)
        return model

    @staticmethod
    def top_decomposition(model:Model, sigmacut:float = 0.005, mingap:float = 5.)-> TopologyList:
        """
        Returns and instance of the SModelS Topology list class that
        represents an iterable collection of topologies.

        Model: - SModelS Model class initiated at model point (slha)
        sigmacut: minimum sigma*BR to be generated, by default sigmacut = 0.1 fb
        mingap: maximum value (in GeV) for considering two R-odd particles degenerate
        """
        sigmacut *= fb
        mingap *= GeV
        toplist = decompose(model, sigmacut, doCompress=True,
                                       doInvisible=True, minmassgap=mingap)
        return toplist

    @staticmethod
    def get_predictions(exp_res:list, tops:TopologyList, usebest:bool, combine:bool=False)-> TheoryPredictionList:
        """
        Returns an instance of the SModels TheoryPredictionList 
        class represents a collection of theory prediction objects.

        expResult: expResult to be considered (ExpResult object), 
                   if list of ExpResults is given, produce theory 
                   predictions for all
        tops: list of topologies containing elements (TopologyList object)
        usebest: Use best dataset as defined by SModelS
        combine: Combine analyses (NOT SIGNAL REGIONS!!)
        """
        return theoryPredictionsFor(exp_res, tops, useBestDataset=usebest, combinedResults=combine)

    def _get_predictions(self, exp_res:list, slhafile:str, combine:bool=True, usebest:bool=False)-> TheoryPredictionList:
        """
        Returns a single instance of a SModels TheoryPrediction class

        expResult: expResult to be considered (ExpResult object), 
                   if list of ExpResults is given, produce theory 
                   predictions for all
        slhafile: path to slha file that defines the model
        combine: Combine signal regions, calls SModels TheoryPredictionsCombiner
        usebest: Use best dataset as defined by SModelS returns TheoryPrediction
        """
        model = self.set_model(slhafile)
        self.tops = self.top_decomposition(model)
        theoryPredictions = self.get_predictions(exp_res, self.tops, usebest=usebest, combine=False)
        if combine:
            return TheoryPredictionsCombiner(theoryPredictions, slhafile=slhafile, marginalize=False)
        else:
            if theoryPredictions:
                return theoryPredictions[0]
            else:
                return theoryPredictions

    def r_value(self, slhafile: str, anas_and_sr : dict, database:Database=None, 
                combine:bool=True, bestSet:bool=False, verbose:bool=False)-> np.ndarray:
        """calculate r value for combined signal regions"""

        if database is None:
            dbpath = "official"
            database = Database( dbpath )

        exp_res = self.exp_results(database, anas_and_sr)

        if exp_res is None:
            tpreds = None
        else:
            tpreds = self._get_predictions(exp_res, slhafile, combine=combine, usebest=bestSet)

        if verbose:
            print (tpreds)
        robs, rexp = np.NaN, np.NaN
        if tpreds:
            robs = tpreds.getRValue ( expected=False )
            rexp = tpreds.getRValue ( expected=True )

        if verbose:
            print ( f'robs: {robs}, rexp: {rexp}' )
        return np.array([robs, rexp])

    def chi_value(self, slhafile: str, anas_and_sr : dict, database:Database=None, expected: bool=False, combine:bool=True, 
                  bestSet:bool=False, verbose:bool=False, anomaly_mode:bool=False, r_value:bool=False)-> np.ndarray:
        """calculate p value for combined signal regions"""
        if database is None:
            dbpath = "official"
            database = Database( dbpath )

        exp_res = self.exp_results(database, anas_and_sr)

        if exp_res:
            predictions = self._get_predictions(exp_res, slhafile, combine=combine, usebest=bestSet)
        else:
            predictions = None

        if verbose:
            print ( "predictions", combine, predictions)

        chi2, likelihood, lmax, rval = np.NaN, np.NaN, np.NaN, np.NaN

        if predictions:
            #print(pred.dataType())
            #print(pred)
            #print ( "Lmax", pred.lmax, "l", pred.likelihood, "Lsm", pred.lsm )

            ## now here is the fun part, the theory prediction object "pred" knows 
            ## the likelihood of the model (pred.likelihood), the likelihood at 
            ## the signal strength that maximizes the likelihood (lmax), and the 
            ## likelihood of the signal strength = 0., aka the standard model (lsm)
            try :
                if anomaly_mode:
                    predictions.computeStatistics(expected=False)
                    lmax = predictions.lmax(expected=False)
                    likelihood = predictions.lsm(expected=False)
                else:
                    predictions.computeStatistics(expected=expected)
                    lmax = predictions.lmax(expected=expected)
                    likelihood = predictions.likelihood(expected=expected)
            except SModelSExperimentError:
                print('Error in likelihood')
            if r_value:
                try:
                    rval = predictions.getRValue (expected=expected)
                except SModelSExperimentError:
                    print('Error in R-value')

            if likelihood > 0.0 and lmax > 0.0:
                chi2 = -2.0 * np.log ( likelihood / lmax )

            elif likelihood == 0.0 and lmax == 0.0:
                chi2 = np.NaN

            elif likelihood == 0.0:
                chi2 = np.inf

            else: # lmax == 0.0:
                chi2 = -np.inf

            if verbose:
                print('dataSet: ', predictions)
                print ( "chi2 = ", chi2)           

        return np.array([chi2, likelihood, lmax, rval])

    @staticmethod
    def chi_to_p(ch2, df=1):
        #return scipy.stats.distributions.chi2.sf (chi2, df=df)
        return chi2.sf (ch2, df=df)

    @staticmethod
    def covariance(exp_res) -> np.ndarray:
        """ return covarience """
        if exp_res is None:
            print('Experimental result not set!!!')
        else:
            return np.array(exp_res.globalInfo.covariance)
        return np.array([])

def format ( r ):
    v = [ "%.3f"  % x for x in r ]
    return f"(robs={v[0]}, rexp={v[1]}, chi2={v[2]})"

def testOnePoint():
    #slhafile = "111928145.slha"
    #anas_and_sr = {'ATLAS-SUSY-2013-02': ['SR2jt'], 'CMS-SUS-13-012': ['3NJet6_500HT800_600MHTinf', '6NJet8_500HT800_450MHTinf'], 'ATLAS-SUSY-2016-07': ['2j_Meff_2400'], 'CMS-SUS-19-006-ma5': ['SR76', 'SR26', 'SR120'], 'CMS-SUS-19-006': ['SR76', 'SR26', 'SR120']}
    slhafile = "114117515.slha"
    anas_and_sr = {'CMS-SUS-13-012': ['3NJet6_1000HT1250_600MHTinf', '3NJet6_1000HT1250_450MHT600', '6NJet8_1500HTinf_300MHTinf', '3NJet6_1250HT1500_300MHT450'], 'ATLAS-SUSY-2016-07': ['6j_Meff_1800'], 'CMS-SUS-19-006': ['SR72', 'SR145']}
    csr = CombineSignalRegions()
    print ( "[taco] testing a single point!" )
    db = Database ( "official+nonaggregated+fastlim" )
    chi,llhd,lmax = csr.chi_value ( slhafile, anas_and_sr, expected=True, combine=False, verbose=True, database = db )
    pbest = csr.chi_to_p(chi, df=1 )
    print ( f"[taco] p-value of best is {pbest}" )
    chi,llhd,lmax = csr.chi_value ( slhafile, anas_and_sr, expected=True, combine=True, verbose=True )
    pcombined = csr.chi_to_p(chi, df=1 )
    print ( f"[taco] p-value of combined is {pcombined}" )
    assert pcombined <= pbest, f"pcombined({pcombined}) > pbest({pbest})!"
    
def example():
    # examplary combinations
    csr = CombineSignalRegions()
    print ( "[taco] compute r value for single result not combined" )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750" ] }, combine = False )
    print ( f"[taco] values for single result not combined are {format(r)}" )
    print ()
    print ( "[taco] compute r value for single result, combined:" )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750" ] } )
    print ( f"[taco] values for single result, combined, are {format(r)}" )
    print ( )
    print ( "[taco] compute r value for two SRs:" )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750", "SR1_Njet2_Nb0_HT500_MHT500" ] } )
    print ( f"[taco] values for two SRs are {format(r)}" )
    print ( )
    print ( "[taco] compute r value for two strong SRs:" )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750", "SR11_Njet7_Nb1_HT300_MHT300" ] } )
    print ( f"[taco] values for two SRs are {format(r)}" )
    print ( )
    print ( "[taco] compute r value, for 8 + 13:" )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750", "SR1_Njet2_Nb0_HT500_MHT500" ], "CMS-SUS-13-013": [ "SR22_HighPt" ] } )
    print ( f"[taco] values for 8+13 are {format(r)}" )
    print ( )
    print ( "[taco] compute r value, for many SRs:" )
    r = csr.r_value ( "test.slha", { "CMS-SUS-16-033": [ "SR12_Njet5_Nb1_HT750_MHT750", "SR1_Njet2_Nb0_HT500_MHT500", "SR8_Njet5_Nb3_HT500_MHT500" ], "CMS-SUS-13-013": [ "SR21_HighPt", "SR22_HighPt", "SR28_HighPt" ] } )
    print ( f"[taco] values for many SRs are {format(r)}" )

if __name__ == "__main__":
    # testOnePoint()
    example()
