#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
from __future__ import print_function
#from typing import Tuple
import copy
import pandas as pd
import numpy as np
from smodels.experiment.databaseObj import Database
from smodels.theory.theoryPrediction import theoryPredictionsFor
from smodels.theory.decomposer import decompose
from smodels.particlesLoader import BSMList
from smodels.share.models.SMparticles import SMList
from smodels.theory.model import Model
from smodels.tools.physicsUnits import GeV, TeV, fb, pb


class SortSignalRegions():
    """
    Class to collect and organise data from SModelS Database for given a:

    - List analyses (via initiation)
    - Labeled correlation matrix via "self.get_correlations(path)"

    Data should be Labeled via SModels "analysisId:dataId". If labels do
    not match a dictionary can be provided via "match_correlations()"
    """
    def __init__ (self, base_obj:Database=None, basePath: str="official", analysisId:list = None, fl: str=None, 
                  topology: tuple=('all',), all_data:bool=False) -> None:
        """
        basePath:         Absoulute path to sophies_world.pcl
        analysisID:       List of strings containing names of the analysis data to include
        fl:               Force load extension if for base path file type i.e. '.pkl'
        all_data:         For full data including yields
        """
        if base_obj is None:
            self.dbase = Database(basePath, force_load=fl, discard_zeroes=True)
        else:
            self.dbase = base_obj
        if analysisId is None:
            self.analysis_id =self.get_all_analysis(topology)
        else:
            self.analysis_id = analysisId

        self.topology = topology
        # define correlation Attribute
        self.__correlations = None #class version
        self.correlations = None  # User version
        # generate a full set of efficiencyMap data from data base
        self.__master_data = self.get_emap_data(full=all_data) #class version
        self.master_data = copy.deepcopy(self.__master_data) # User version
        self.__correlations_matched = False
        self.__scaled = False
        self.__ref = 'SMS_label'

    @property
    def sr_names(self) -> list:
        "return reference lables "
        return self.master_data[self.__ref].to_list()

    @property
    def sr_upperlimit(self) -> np.ndarray:
        "return upper limit values"
        return self.master_data['expectedUpperLimit'].values
    
    @property
    def available_alayses(self) -> set:
        "return available analyses"
        return set(self.master_data['AnalysisId'].to_list())
    
    @property
    def exp_results(self) -> list:
        """
        returns: list of ExpResult objects or the ExpResult 
        object if the list contains only one result 
        """
        return self.dbase.getExpResults(analysisIDs=self.analysis_id,
                                        txnames=self.topology,
                                        dataTypes = ['efficiencyMap'],
                                        useNonValidated=False, 
                                        onlyWithExpected=False)

    def reset(self, scaled=False) -> None :
        """Resets master & correlation data to initial state """
        self.master_data = copy.deepcopy(self.__master_data)
        self.correlations = copy.deepcopy(self.__correlations)
        self.__scaled = scaled

    def get_emap_data(self, full:bool=False) -> pd.DataFrame:
        """
        Retrieve 'efficiencyMap' data from the database (self.dbase)
        full: bool -> for full data including yields set to True
        Returns pandas database object
        """
        df_temp = []
        # get all the efficiency map results

        ## iterate through the list
        for exp_res in self.exp_results:
            ## iterate through the "datasets" ( = signal regions )
            for dset in exp_res.datasets:
                #collect data into list of dictionary objects
                tmp = {}
                # dump all available data into dictionary
                tmp['dataId'] = dset.dataInfo.dataId
                tmp['AnalysisId'] = exp_res.globalInfo.id
                tmp['expectedUpperLimit'] = dset.dataInfo.expectedUpperLimit
                # build SModelS label
                tmp['SMS_label'] = f"{tmp['AnalysisId']}:{tmp['dataId']}"
                if full:
                    tmp['observedN'] = dset.dataInfo.observedN
                    tmp['expectedBG'] = dset.dataInfo.expectedBG
                    tmp['bgError'] = dset.dataInfo.bgError
                    tmp['lumi'] = exp_res.globalInfo.lumi
                    tmp['sqrts'] = exp_res.globalInfo.sqrts
                    #tmp['upperLimit'] = dset.dataInfo.upperLimit
                df_temp += [tmp]

        return pd.DataFrame(df_temp)

    def efficiency_bool(self, massvalues: list) -> bool:
        """
        !!!For Topology specific analysis!!!!
        Check if an efficiency map is available for a given mass point
        massvalues: list -> list of mass values specific to topology of interest
        """
        exp_results = self.dbase.getExpResults(analysisIDs=self.analysis_id,
                                               txnames=self.topology,
                                               dataTypes='upperLimit')
        if exp_results:
            all_er = [er.getEfficiencyFor(mass=massvalues, txname=self.topology) for er in exp_results]
            return any(all_er)
        return False

    def get_epsilon(self, massvalues: list) -> np.ndarray:
        """
        !!!For Topology specific analysis!!!!
        get efficiency For a given mass value
        massvalues: list -> list of mass values specific to topology of interest
        """
        #temp = {item: 0.0 for item in self.master_data['SMS_label']}
        temp = dict.fromkeys(self.master_data['SMS_label'], 0.0)

        k = 0
        for e_r in self.exp_results:
            dsnames = [ x.dataInfo.dataId for x in e_r.datasets ]
            for dsn in dsnames:
                eff = e_r.getEfficiencyFor (mass=massvalues, txname=self.topology, dataset=dsn )
                sms_lab = f"{e_r.globalInfo.id}:{dsn}"
                if sms_lab in temp:
                    temp[sms_lab] = eff

        ret = np.array([temp[item] for item in self.master_data['SMS_label']])
        ret[(ret == 0.0)] = np.NaN
        return ret

    @staticmethod
    def find_unit_nan(data)->np.ndarray:
        idx = []
        for i, dat in enumerate(data):
            if np.isnan(dat.asNumber()):
                idx.append(i)
        return idx

    def sale_by_efficiency(self, massvalues: list, drop: bool=True, verbose: bool=False) -> None:
        """
        massvalues: list of mass values specific to topology of interest
        drop:       bool default=True, drop any nan values returned in calculation
        verbose:    bool, debug flag for the resorting of the corelation matrix

        To quote Ronseal: 'Does exactly what it says on the tin!'
        scales the expected Upper Limit by the efficiency
        """
        
        if not self.__scaled:
            ep = self.get_epsilon(massvalues)
            self.master_data['expectedUpperLimit'] /= ep
            # drop any NAN values (cant use pd.dropna as as target units are fb)
            if drop:
                drops = self.find_unit_nan(self.master_data['expectedUpperLimit'].to_numpy())
                self.master_data.drop(drops, axis=0, inplace=True)
                self.master_data.reset_index(drop=True, inplace=True)
                self.sort_correlations_by_master(verbose=verbose)
            self.__scaled = True
    
    def get_sigma_n(self, masses:list, xsecs:dict)->None:
        """
        Calculate Sigma N by way of X-sec * luminosity * efficiency
        masses: list of mass values as per model requiremen
        xsects: dict {int(sqrtS): x-sec}
        Adds column to masterdata frame -> self.master_data['sigma_N']
        """
        if 'sqrts' not in self.master_data:
            print("Class not initiated with full data")
            raise NotImplementedError
        xsecs = np.array([xsecs.get(int(sqrts/TeV), np.nan*pb) for sqrts in self.master_data['sqrts']])
        sigN = xsecs * self.master_data['lumi'].to_numpy() * self.get_epsilon(masses)
        self.master_data['sigma_N'] = np.array([sn.asNumber() for sn in sigN])    
        #drops = self.find_unit_nan(self.master_data['sigma_N'].to_numpy())
        #self.master_data.drop(drops, axis=0, inplace=True)
        self.master_data.dropna(axis=0, subset=['sigma_N'], inplace=True)
        self.master_data.reset_index(drop=True, inplace=True)
        self.sort_correlations_by_master(verbose=False)

    def get_correlations(self, correlation_loc: str, absolute=True) -> None:
        """
        - correlation_loc:  Absolute path to the correlation data
        - absolute:         Use absolute values
        Load correlation data. File must be N x N text file with:
        [0, :] = [:, 0] = data labels
        """
        if  absolute:
            self.__correlations = pd.read_csv(correlation_loc, index_col=0).abs()
        else:
            self.__correlations = pd.read_csv(correlation_loc, index_col=0)
        
        self.correlations = copy.copy(self.__correlations)

    def sort_square_by_names(self, square_df: pd.DataFrame, names: list) -> pd.DataFrame:
        """
        - square_df:    N x N data frame with matching colum and index labels
        - names:        list of labels to oder the df by

        Sort any square data frame (column and index)
        into label name order automatically removing
        correlations that don't appear in name list.
        Returns reordered dataframe
        """
        order = []
        labs = []
        drop_list = []
        #idcorr = [*square_df]  #labels in correlations
        idcorr = {col: i for i, col in enumerate(square_df.columns)}
        #get index in corelations labels that match master labels
        for i, name in enumerate(names):
            if name in idcorr:
                #order += [idcorr.index(name)]
                order += [idcorr[name]]
                labs += [name]
            else:
                drop_list += [i]
    
        if drop_list:
            self.drop_by_index(drop_list, verbose=False, sort_corr=False)

        #get numeric values from correlations and re-order in both axis
        matrix = square_df.values[order, :][:, order]

        return pd.DataFrame(matrix, index=labs, columns=labs)

    def sort_correlations_by_master(self, verbose: bool=False) -> None:
        """
        - verbose: Print out final shapes (sanity check)

        Sort the correlations data frame (column and index)
        into master label order using sort_square_by_names
        """

        self.correlations = self.sort_square_by_names(self.correlations,
                                                        self.master_data[self.__ref].to_list())
        

        if verbose:
            print('\n------------- Data Matched ------------')
            print('Master Data Frame Length:        {}'
                  .format(self.master_data.shape[0]))
            print('Correlation Data Frame Shape:    {}'
                  .format(self.correlations.shape))
            print('Trace of Correlations:           {}\n'
                  .format(np.trace(self.correlations.values)))

    @staticmethod
    def _get_translation() -> dict:
        from . import dictionary as dct
        return dct.get_trans()

    def translate_reference(self, translation_loc: str, replace: list=None):
        translation = self._get_translation()
        #print(translation)
        trans_keys = []
        not_found = []
        track = {}
        for i, item in enumerate(self.master_data[self.__ref]):

            if item in translation:
                label = translation[item]
                if replace is not None:
                    for rep in replace:
                        label = label.replace(*rep)
                trans_keys.append(label)
                #print(f'True {i}: {item}')
            else:
                #print(f'False {i}: {item}')
                trans_keys.append(np.nan)
                not_found += [item]

        if not_found:
            print("SR's Not Found In Dictionary")
            for n_f in not_found: print(n_f)

        self.__ref = 'translation'
        self.master_data[self.__ref] = trans_keys
        self.master_data.dropna(axis=0, how='any', subset=[self.__ref], inplace=True)
        self.master_data.reset_index(drop=True, inplace=True)

    def match_correlations(self, correlation_loc: str, translation_loc: str=None,
                           replace: list=None) -> None:
        """
        - correlation_loc:              Absolute path to the correlation data
        - translation_loc(optional):    Absolute path to the translation dictionary
        - replace (optional):           For translation -> see "transform_label" method

        Match entries from master_data (dataFrame) to the correlation (dataFrame).
        In case of non matching lables, a translation dictionary can be provided
        via the path variable "translation_loc", this must lead to a file that contains
        a string format python dictionary i.e. '{ 'SModels Label' : 'MG5 Label' , ...}'.
        """
        print('\nMatching data to corelation matrix')

        #load the correlation matrix information
        self.get_correlations(correlation_loc)
        #load translation dictionary if required
        if translation_loc is not None:
            self.translate_reference(translation_loc, replace=replace)
        #self.master_data.drop_duplicates(subset=[self.__ref], inplace=True, ignore_index=True)
        self.sort_correlations_by_master(verbose=True)
        #make sure that there are no repeat's
        #self.drop_repeated()
        #update matched status

        self.drop_repeated(verbose=False)
        self.__correlations_matched = True
        #update the private master copies
        self.__master_data = copy.deepcopy(self.master_data)
        self.__correlations = copy.deepcopy(self.correlations)


    def drop_signal_region(self, sr_name: str, verbose: bool=True) -> None:
        """
        - sr_name:  (string) name of the signal region label to drop
        Drops signal region from master data and correlation data
        """
        index = self.master_data[self.__ref].to_list().index(sr_name)
        self.drop_by_index(index, verbose=verbose)

    def drop_by_index(self, index: list, verbose: bool=False, sort_corr: bool=True) -> None:
        """
        - index: list of indices to drop
        Drop data according to index in Dataframe
        """
        self.master_data.drop(self.master_data.index[index], inplace=True)
        self.master_data.reset_index(drop=True, inplace=True)
        if sort_corr:
            self.sort_correlations_by_master(verbose=verbose)

    def drop_repeated(self, col:str=None, verbose: bool=False) -> None:
        """
        To quote Ronseal: 'Does exactly what it says on the tin!'
        drops any repeated analysis from master data frame
        NOTE: not sure if this is needed..... can't hurt.
        """
        if col is None:
            col = self.__ref
        self.master_data.drop_duplicates(subset=[col], keep='first', inplace=True)
        self.master_data.reset_index(drop=True, inplace=True)
        #self.correlations.drop_duplicates(keep='first', inplace=True)
        self.sort_correlations_by_master(verbose=verbose)

    def drop_nan(self, subset=None, verbose: bool=False):
        self.master_data.dropna(subset=[subset], inplace=True)
        self.master_data.reset_index(drop=True, inplace=True)
        self.sort_correlations_by_master(verbose=verbose)

    def drop_value(self, subset:str, val:float=0.0, verbose:bool=False):
        #self.master_data[subset][(self.master_data[subset] == val)] = np.NaN
        self.master_data[subset]=self.master_data[subset].replace(0, np.NaN)
        self.drop_nan(subset=subset, verbose=verbose)

    def sort_by_upper_limit(self) -> None:
        """ calls sort by method for the case of expected upper limit """
        self.sort_by('expectedUpperLimit', asc=True)

    def sort_by_r_value(self, slhafile: str, expected: bool, cut_value: float=None) -> None:
        """
        slhafile: absoulte path to slha file.
        exspected: bool, chose expected (True) or observed (False) r-value
        cut_value: optional upper bound i.e. drop all values above cut_value=1.0

        calculates and sorts by r-value
        """

        # chose expected or observed for column label
        if expected:
            col_lab = 'r_exp'
        else:
            col_lab = 'r_obs'
        #calculate r-values
        self.master_data[col_lab] = self.r_value(slhafile, expected)
        # apply cut
        if cut_value is not None:
            idx = [i for i, item in enumerate(self.master_data[col_lab]) if item > cut_value]
            if idx:
                self.drop_by_index(idx, verbose=False)
        #sort
        self.sort_by(col_lab, asc=False)

    def sort_by(self, column: str, asc: bool=True) -> None:
        """ Generalised sorting method """
        self.master_data.sort_values([column], axis=0, ascending=asc, inplace=True)
        self.master_data.dropna(axis=0, how='any', subset=[column], inplace=True)
        self.master_data.reset_index(drop=True, inplace=True)
        self.sort_correlations_by_master(verbose=False)

    def r_value(self, slhafile: str, expected: bool = True) -> np.ndarray:
        """
        slhafile: absoulte path to slha file
        exspected: bool, chose expected (True) or observed (False) r-value

        Calculate r-value for each signal region
        return array of r-values in matched order of master data frame id's
        """
        #temp = {item: np.NaN for item in self.master_data['SMS_label']}
        temp = dict.fromkeys(self.master_data['SMS_label'], np.nan)
        exp_results = self.dbase.getExpResults(analysisIDs=self.analysis_id,
                                               txnames=self.topology,
                                               dataTypes = ['efficiencyMap'] )
        model = Model(BSMparticles=BSMList, SMparticles=SMList)
        model.updateParticles(inputFile=slhafile )
        smstopos = decompose ( model )
        dids = self.master_data['dataId'].to_list()
        ## iterate through the list
        for exp_res in exp_results:
            ## iterate through the "datasets" ( = signal regions )
            tpred = theoryPredictionsFor (exp_res, smstopos, combinedResults=False,
                                              useBestDataset=False, marginalize=False )
            if tpred is not None:
                for t_p in tpred:
                    data_id = t_p.dataset.dataInfo.dataId
                    sms_lab = f"{exp_res.globalInfo.id}:{data_id}"
                    temp[sms_lab] = t_p.getRValue ( expected=expected )

        r_values = np.array([temp[item] for item in self.master_data['SMS_label']])
        return r_values
    
    def rank_signal_region(self, correlation_loc: str=None, resort_master: bool=False,
                           sort_method: str='eUL', t_h: float=0.1, sort_kwargs: dict=None,
                           sp: int=0, verbose: bool=True) -> list:
        """
        correlation_loc:    Absolute path to the correlation data
        resort_master:      Option to reorder the master and corelation data wrt the results
        sort_method:        Sorting method to use, eUL is expected upper limit.
        t_h:                Threshold to apply to the corelation matrix

        Rank signal regions using combination of sorting method and corelations.
        see 'apply_correlations' for sorting algorithm.

        Returns a list of signal regions in order or ranking (optimum = [0])
        """
        # check if master data has been matched against correlation labels
        if not self.__correlations_matched:
            if correlation_loc is None:
                print('No correlation information provided')
                #return False
            else:
                self.match_correlations(correlation_loc)
        if not self.__correlations_matched:
            return False
        # initiate sorting by expected upper limit

        if sort_method == 'eUL':
            if verbose:
                print('Sorting by Expected Upper Limit.\n')
            self.sort_by_upper_limit()

        elif sort_method == 'r-value':
            if verbose:
                print('Sorting by r-value.\n')
            if sort_kwargs is None:
                print('No arguments provided for r value calculation')
            else:
                self.sort_by_r_value(**sort_kwargs)
        # option to add alternative
        else:
            print('No recognised pre-sorting method requested.\n')

        #apply the corselation ranking
        opt_sr_name, opt_sr_index = self.apply_correlations(threshold=t_h, start=sp)

        if resort_master:
            self.master_data = self.master_data.iloc[opt_sr_index]
            self.master_data.reset_index(drop=True, inplace=True)
            self.sort_correlations_by_master(verbose=False)

        return opt_sr_name

    def set_llr_at_point(self, slhafile:str, expected:bool=True, chi_factor:bool=True, 
                         drop_nan:bool=True, drop_zero:bool=False, anomaly_mode:bool=False)-> None:
        """
        Calculate the log likelihood ratio for each SR as specified by model point.
            slhafile:   str path to slha file
            expected:   bool expected (True) or observed (False)
            chi_factor: bool multiply LLR by -2.0 for NLLR 
            drop_nan:   bool drop master data row if nan returned 
        Sets new column in master data -> self.master_data['llr']
        """
        if chi_factor:
            pre = -2.0
        else:
            pre = 1.0

        model = Model(BSMparticles=BSMList, SMparticles=SMList )
        model.updateParticles(inputFile=slhafile)
        toplist = decompose(model, 0.005*fb, doCompress=True,
                                       doInvisible=True, minmassgap=5.*GeV)

        llr = dict.fromkeys(self.master_data['SMS_label'], np.nan)

        for e_r in self.exp_results:
            predictions = theoryPredictionsFor(e_r, toplist, useBestDataset=False,
                                            combinedResults=False, marginalize=False)
            if predictions is not None:
                for pred in predictions:
                    tag = f"{e_r.globalInfo.id}:{pred.dataset.dataInfo.dataId}"
                    if tag in llr:
                        
                        if anomaly_mode:
                            pred.computeStatistics(expected=False)
                            likelihood = pred.lsm(expected=False)
                            lmax = pred.lmax(expected=False)
                        else:
                            pred.computeStatistics(expected=expected)
                            likelihood = pred.likelihood(expected=expected)
                            lmax = pred.lmax(expected=expected)
                            
                        if likelihood is not None and lmax is not None:
                            if likelihood > 0.0 and lmax > 0.0:
                                llr[tag] = pre * np.log ( likelihood / lmax )
                            else:
                                llr[tag] = np.NaN

        self.master_data['llr'] = np.array([llr[i_d] for i_d in  self.master_data['SMS_label']])
        if drop_nan:
            self.drop_nan(subset='llr')
        if drop_zero:
            self.drop_value(subset='llr', val=0.0, verbose=False)


    def order_results(self, results: list) -> dict:
        """
        Helper function to convert results into dictionary format: 
            results: list of reference labels or indexes to match to master data
            returns: result dictionary i.e. {analysis ID: [sr0, sr1, ... srN]}
        """
        if all(isinstance(x, (int, np.int64)) for x in results):
            res_index = results
        
        elif all(isinstance(x, str) for x in results):
            res_index = self.name_to_index(results)
        else:
            raise ValueError('order_results: results of wrong or mixed type')

        return self.index_to_result(res_index)
    
    def name_to_index(self, labels:list, ref: str=None) ->list:
        """
        convert list of data lables into coresponding index
            lables : list -> data ID's or custom if set
            ref : master_data column name that contains labels
        """
        if ref is None or ref not in self.master_data:
            names = [*self.correlations]
        else:
            names = self.master_data[ref].to_list()
      
        return [names.index(lab) for lab in labels if lab in names]

    def index_to_name(self, indxes:list, ref:str= None) -> list:
        """
        convert list of indexes into coresponding data lables
            indexes : list -> data indexes
            ref : master_data column name that contains labels
        """
        if ref is None or ref not in self.master_data:
            names = [*self.correlations]
        else:
            names = self.master_data[ref].to_list()

        return [names[i] for i in indxes]

    def index_to_result(self, indxes:list) -> dict:
        """
        convert list of indexes into result format
            indexes : list -> data indexes
            returns: results i.e. {analysis ID: [sr0, sr1, ... srN]}
        """
        an_id = self.master_data.loc[indxes]['AnalysisId'].to_list()
        ret = {aid: list() for aid in set(an_id)}
        for aid, idx in zip(an_id, indxes):
            ret[aid].append(self.master_data.loc[idx]['dataId'])
        return ret

    def all_as_result(self):
        """
        convert all current SR's  into result format 
            indexes: list -> data indexes
            returns: results i.e. {analysis ID: [sr0, sr1, ... srN]}
        """
        
        res = self.index_to_result(self.master_data.index)
        return res
    
    def result_to_name(self, result:dict, ref:str=None) -> list:
        """
        convert result dictionary into list of data ID's taken from master_data 
            result : dict {analysis ID: [sr0, sr1, ... srN]}
            ref : master_data column name that contains labels
            returns: list of strings
        """
        indexes = []
        labs = self.master_data['dataId'].to_list()
        for _, item in result.items():
            if isinstance(item, list):
                for name in item:
                    if name in labs:
                        indexes += [labs.index(name)]
                    else:
                        print(f'{name} not found')
            else:
                if item in labs:
                    indexes += [labs.index(item)]
                else:
                    print(f'{name} not found')

        return self.index_to_name(indexes, ref=ref)
