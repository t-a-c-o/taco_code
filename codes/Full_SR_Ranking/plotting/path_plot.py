import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns; sns.set_theme()

from matplotlib.pyplot import cm
import matplotlib.offsetbox as offsetbox
from matplotlib.patches import Patch, Rectangle

plt.rcParams['legend.handlelength'] = 1
plt.rcParams['legend.handleheight'] = 1

plt.rcParams['savefig.dpi'] = 300
#plt.rcParams['text.usetex'] = True
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] =  ["Palatino"]
#plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["axes.grid"] = False

#plt.rcParams.update({'font.family':'Arial'})
#plt.rcParams.update({'font.sans-serif':'Helvetica'})

class PathPlot():
    
    @staticmethod
    def _axtp(ax, fs=12, c='k', lib_dir='out', lft=True, rgt=False):
        """axis formatting"""

        ax.tick_params(direction=lib_dir, which='major', length=10, width=1, labelsize=fs,
                       bottom=True, top=False, left=lft,  right=rgt, colors=c)
        ax.tick_params(direction=lib_dir, which='minor', length=5, width=1,
                       bottom=True, top=True, left=lft, right=rgt, colors=c)
        return(ax)

    @staticmethod
    def color_value(c_vals, val):
        return abs(c_vals - val).argmin()

    @staticmethod
    def set_diagonal(df, val=0.5):
        if type(df) != np.ndarray:
            data = df.to_numpy(dtype=np.float64)
            labs = [*df]
        else:
            data = np.array(df, dtype=np.float64)
            labs = ['temp']*len(df)
        for k in range(len(data)):
            data[k, k] = val
        return pd.DataFrame(data, columns=labs, index=labs)
 

    @staticmethod
    def corner_mask(msked_sig):
        corner = np.ones(msked_sig.shape, dtype='bool')
        for i in range(msked_sig.shape[1]):
            for j in range(msked_sig.shape[0]):
                if j >= i:
                    corner[j, i] = False
        return corner

    @staticmethod
    def _forward(x):
        return x**(2)

    @staticmethod
    def _inverse(x):
        return x**(1/2)
    
    @staticmethod
    def triangle_index(num):
        ret = []
        sub = []
        n = 0
        for i in range(num):
            n += 1
            sub += [i]
            if 0.5*n*(n+1) > num:
                ret.append(sub)
                sub = []
                num -= n
                n=0
        return ret

    def text_triangle(self, labs):
        ret = ' '
        num = len(labs)
        order = self.triangle_index(num)
        inset = 0
        for i, item in enumerate(order):
            inset += len(labs[item[0]])
            for idx in item:
                ret += labs[idx]
            ret += '\n'
            if i < len(order) - 1:
                ret += inset * '_'
        return ret
    @staticmethod
    def weight_sum(wghts):

        return np.array([np.asarray(w).sum() for w in wghts])

    @   staticmethod
    def decompose_path(path) -> dict:
        steps = []
        for i in range(1, len(path)):
            steps.append(path[:i+1:])
        return steps

    @staticmethod
    def format_path(path:list)->np.ndarray:
        #[0, 1, 2, 3]
        #x = [[0, 1], [1, 1], [1, 2], [2, 2], [2, 3], [3, 3]]
        #y = [[0, 0], [0, 1], [1, 1], [1, 2], [2, 2], [2, 3]]
        x = []
        y = []
        for i, j in zip(path[:-1:], path[1::]):
            x += [[i, i], [i, j]]
            y += [[i, j], [j, j]]
        return np.array([x, y])

    def make_path(self, paths:list, weights:list, shift=True)->dict:
        shift_i = 1./(len(paths) + 1.)
        lshft = 0.5
        c_vals = np.linspace(0.0, 1, 101)
        color = cm.rainbow(c_vals)
        ret = {}
        for i, (w, p) in enumerate(zip(weights, paths)):
            n = 0
            ret[i] = {}
            posx = []
            posy = []
            colors = []
            if shift:
                lshft = (i+1)*shift_i
            lt = w[n]
            pth = self.format_path(p)
            for j, k in zip(pth[0], pth[1]):
                xy = np.array([j, k])
                loc = self.color_value(c_vals, lt)
                if xy[0][0] != xy[0][1]:
                    n += 1
                    if n < len(weights[i]):
                        lt = w[n]
            
                posx += [xy[0]+lshft]
                posy += [xy[1]+lshft]
                colors += [color[loc]]
            ret[i]['x'] = posx
            ret[i]['y'] = posy
            ret[i]['color'] = colors
        return ret

    def plot(self, corr_df, path, weight=None, th=0.01, spath=None, shift=True, s_bar=True, dule=False, anim=False, new_labs=None):

        msked_sig = (corr_df < th).astype(int)
        mask = self.corner_mask(msked_sig)
        uniform_data = self.set_diagonal(msked_sig, val=0.5)
        fs = 38
        old_labs = [*uniform_data]
        if new_labs is None:
            new_labs = ['SR' + r'$_{}$'.format(str(i)) for i in range(len(old_labs))] 
        uniform_data = pd.DataFrame(uniform_data.values, index=new_labs, columns=new_labs)
        sns.set_theme()
        
        if dule:
            if s_bar:
                _, axis = plt.subplots(nrows=1, ncols=2, figsize=(24,12), sharey=True)
            else:
                _, axis = plt.subplots(nrows=1, ncols=2, figsize=(20,10), sharey=True)
            ax_0, ax = axis[0], axis[1]

            p0map = sns.heatmap(uniform_data, cmap=['k', 'grey', 'w'], ax=ax_0, annot=False, fmt='.2f', 
                           cbar=False, mask=mask, linewidths=4, linecolor='k')
            p0map.set_xticklabels(p0map.get_xticklabels(), rotation=90)
            p0map.set_yticklabels(p0map.get_yticklabels(), rotation=0)

            pmap = sns.heatmap(uniform_data, cmap=['k', 'grey', 'w'], ax=ax, annot=False, fmt='.2f', 
                           cbar=False, mask=mask, linewidths=4, linecolor='k')
            pmap.set_xticklabels(pmap.get_xticklabels(), rotation=90)
            ax_0 = self._axtp(ax_0, fs=0.5*fs, c='k')
            ax = self._axtp(ax, fs=0.5*fs, c='k', lft=False)
        else:
            if s_bar:
                fig, ax = plt.subplots(figsize=(12,8))
            else:
                fig, ax = plt.subplots(figsize=(10,8))
                ax = self._axtp(ax, fs=0.5*fs, c='k')
            pmap = sns.heatmap(uniform_data, cmap=['k', 'grey', 'w'], ax=ax, annot=False, fmt='.2f', 
                           cbar=False, mask=mask, linewidths=2, linecolor='k')
            pmap.set_xticklabels(pmap.get_xticklabels(), rotation=0)
            pmap.set_yticklabels(pmap.get_yticklabels(), rotation=0)
        #legend ------
        legend_elements = [Rectangle((0.0, 0.0), 1, 2, facecolor='w', edgecolor='k',
                                 label=r'$\rho_{ij} < T$'),
                           Rectangle((0.0, 0.0), 1, 2, facecolor='k', edgecolor='k',
                                 label=r'$\rho_{ij} \geq T$'),
                           Rectangle((0.0, 0.0), 1, 2, facecolor='grey', edgecolor='k',
                                 label=r'$\rho_{ii}$')
                          ]
        ax.legend(handles=legend_elements, loc='upper right', fontsize='30', framealpha=1)
        #-------------
        if path:
            if weight is None:
                weight = []
                nums = np.linspace(0, 1, len(path))
                for i, item in enumerate(path):
                    weight.append([nums[i]]*len(item))

            else:
                mw = self.weight_sum(weight).max()
                weight = [np.cumsum(np.asarray(w))/mw for w in weight]
            if anim:
                count = 0
            for i, item in self.make_path(path, weight, shift=shift).items():
                if anim:
                    x_anim, y_anim  = [], []
                for x, y, c in zip(item['x'], item['y'], item['color']):
                    if anim:
                        
                        x_anim += list(x)
                        y_anim  += list(y)
                        ax.plot(x_anim, y_anim, lw=3, color=c, linestyle='-')
                        spth = f'{spath.split(".png")[0]}_{count}.png' 
                        plt.tight_layout()
                        plt.savefig(spth)
                        count += 1
                    else:
                        ax.plot(x, y, lw=3, color=c, linestyle='-') #zorder=i)

        barlab = r'$ \frac{\sum \omega_{sr}}{\omega_{max}} $'
        if s_bar:
            cmap = cm.rainbow
            norm = mpl.colors.Normalize(vmin=0.0, vmax=1)
            #norm = mpl.colors.FuncNorm((self._forward, self._inverse), vmin=0, vmax=1)
            cbar = plt.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap), orientation='vertical')

            cbar.set_label(barlab, fontsize=fs, rotation=0, labelpad=20)
            cbar.ax = self._axtp(cbar.ax, fs=0.5*fs, c='k', lib_dir='out', lft=False, rgt=True)


        plt.tight_layout()

        if spath is not None:
            plt.savefig(spath)
        else:
            plt.show()

        plt.close()