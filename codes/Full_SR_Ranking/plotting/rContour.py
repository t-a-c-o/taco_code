import numpy as np
import numpy.ma as ma
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib.colors as colors
from matplotlib.ticker import MultipleLocator

plt.rcParams['savefig.dpi'] = 300
#plt.rcParams['text.usetex'] = True
plt.rcParams['axes.facecolor'] = 'white'
plt.rcParams["font.family"] = "serif"
plt.rcParams["font.serif"] =  ["Palatino"]
#plt.rcParams["mathtext.fontset"] = "dejavuserif"
plt.rcParams["mathtext.fontset"] = "cm"
plt.rcParams["axes.grid"] = False

# plt.rcParams['xtick.direction']           = 'in'
# plt.rcParams['ytick.direction']           = 'in'
# plt.rcParams['xtick.top']                 = True
# plt.rcParams['xtick.minor.visible']       = True
# plt.rcParams['ytick.right']               = True
# plt.rcParams['ytick.minor.visible']       = True
# plt.rcParams['xtick.labelsize']           = 15
# plt.rcParams['ytick.labelsize']           = 15
# plt.rcParams['axes.labelsize']            = 20



from itertools import product
from scipy.interpolate import CloughTocher2DInterpolator


class SM_Plotting():

    def __init__ (self):
        self.dm = 5

    @staticmethod
    def _axtp(ax, fs:int=12, c:str='k', tick_size:tuple=(4, 1), scale:float=1.0):
        """axis formatting"""

        ax.tick_params(direction='in', which='major', length=tick_size[0], width=tick_size[1], labelsize=fs,
                      grid_linewidth=0.5, bottom=True, top=True, left=True, 
                      right=True, colors=c, grid_color=c)

        ax.xaxis.set_minor_locator(MultipleLocator(25*scale))
        ax.yaxis.set_minor_locator(MultipleLocator(25*scale))
        ax.tick_params(direction='in', which='minor', length=1, width=1,
                      bottom=True, top=True, left=True, right=True, colors=c)

        ax.spines['bottom'].set_color('0.5')
        ax.spines['top'].set_color('0.5')
        ax.spines['right'].set_color('0.5')
        ax.spines['left'].set_color('0.5')

        return(ax)
    
    @staticmethod
    def line_style():
        for ls in {'dotted', 'solid', 'dashed', 'dashdot', 'dotted'}:
            yield ls

    @staticmethod
    def format_arrays(zed_arr, titles=['combined']):

        if zed_arr.ndim == 2:
            zed_arr = zed_arr[np.newaxis, ...]

        if len(titles) != len(zed_arr):
            print('title list is if of wrong length!')
            titles=['combined'] + list(range(len(zed_arr)-1)) 


        return {item: zed for item, zed in zip(titles, zed_arr)}

    @staticmethod
    def fmt(x):
        """string formating"""
        s = f"{x:.1f}"
        if s.endswith("00"):
            s = f"{x:.1f}"
        return rf"{s}" if plt.rcParams["text.usetex"] else f"{s}"

    @staticmethod
    def interpolate_plain(x, y, z, dm, min_val=None, max_val=None):
   
        # Flattened array for interpolation
        zedr = z.ravel()
        # Mask for interpolation
        msk = np.ones(len(zedr), dtype='bool')
        if min_val is not None:
            msk *= (zedr > min_val)
        if max_val is not None:
            msk *= (zedr < max_val)

        # points on plain (grid) matching flattened z 
        mass_points = np.array(list(product(x, y)))

        # new data for interpolation to d(mass) step
        xnew = np.arange(x.min(), x.max()+dm, dm)
        ynew = np.arange(x.min(), x.max()+dm, dm)
    
        X, Y = np.meshgrid(xnew, ynew)

        interp = CloughTocher2DInterpolator(mass_points[msk], zedr[msk])
  
        return X, Y, interp(X, Y)

    def counour_at_level(self, axis, x, y, z, levels, clr):
        cs = axis.contour(x, y , z.T, levels=levels, linestyles='solid', linewidths=2, colors=clr)
        return axis, cs

    def plot_2plain(self, x:np.ndarray, y:np.ndarray, z_dict:dict, axis_lables:list, bar_lab, vmm, 
                    levels=[1.], limits=None, cmap="Purples_r", savefig=False):

        scale = 1e-3
        x, y = x*scale, y*scale

        if cmap is None: 
            figsize = (7, 6)
        else:
            figsize = (8, 6)
        fig, axis = plt.subplots(1, 1, figsize=figsize, sharey=True, constrained_layout=True, facecolor='w')
        fs = 20
        legfs=15
        axis.grid(False)

        #ls = self.line_style()
        ls = iter(['solid', 'dashed', 'dashdot', 'dotted'])
        
        color=iter(cm.Set1(np.linspace(0,1,9)))
        leg_list = []
        for i, (label, zed) in enumerate(z_dict.items()):
            np.nan_to_num(zed, copy=False)
    
            if i == 0:
                X, Y, Z = self.interpolate_plain(x, y, zed, self.dm, min_val=0.0, max_val=None) 
                c_bord = axis.contour(x, y , zed.T, levels=[0], linestyles='--', alpha=0.4, linewidths=2, colors='k')
                clr = 'k'
            else:
                clr = next(color).reshape(-1,4)
            # plot 2D interpolated data
            if cmap is not None:
                pc = axis.pcolormesh(X, Y , Z, vmin=vmm[0], vmax=vmm[1], cmap=cmap, shading='auto')
            # get combined countour

            c_temp = axis.contour(x, y , zed.T, levels=levels, linestyles=next(ls), linewidths=2, colors=clr)
            
            h1 ,_ = c_temp.legend_elements()
            leg_list.append(h1[0])
        if len(bar_lab) == 1:
            ttle = r'Exclusion Limits (r = 1)'
            heat_stick = r'r = $\sigma_{signal}/\sigma_{UL}$'
        else:
            ttle = bar_lab[1]
            heat_stick = bar_lab[2]
        axis.legend(leg_list, [*z_dict], loc=2, fontsize=legfs, title=ttle, title_fontsize=legfs, frameon=False)

        if limits is not None:
            axis.set_xlim(scale*limits[0])
            axis.set_ylim(scale*limits[1])
            
        axis.set_title(bar_lab[0], fontsize=fs, loc='left', pad=20)
            
   
        axis = self._axtp(axis, fs=legfs, c='k', scale=scale)
    
        ticks = [0, 0.5, 1, 1.5, 2, 2.5, 3]

        if cmap is not None:
            cbar = fig.colorbar(pc, orientation="vertical", ax=axis, ticks=ticks)
            cbar.set_label(heat_stick, fontsize=fs)
            cbar.ax = self._axtp(cbar.ax, fs=legfs, c='k')
        axis.set_xlabel(axis_lables[0], loc='right', fontsize=fs)
        axis.set_ylabel(axis_lables[1], loc='top', fontsize=fs)
        

        if savefig:
            plt.savefig(savefig, transparent=False)
        
        #fig.subplots_adjust(hspace=0)
        #plt.tight_layout()

    def plot_bar_count(self, res:list, model:str='Model'):

        from collections import Counter
        #all_analysis = [item for _, subdict in res.items() for item in subdict['a_sr']]
 
        count = Counter(res)
        mc = dict(count.most_common())
        num = len(res)
        fig, ax = plt.subplots(1, 1, figsize=(12, 9), constrained_layout=True, facecolor='w')
        ax.grid(False)
        for key, item in mc.items():
            ax.bar(key, item/num)
        ax.set_ylabel('Fraction')
        ax.set_title(model, fontsize=20)
        ax.tick_params(axis='x', labelrotation=45)
        ax = self._axtp(ax, fs=12, c='k')
        plt.show()
    
    def plot_pie_count(self, res:list, model:str='Model', savefig:str='Figs/pie.png'):

        from collections import Counter
        #all_analysis = [item for _, subdict in res.items() for item in subdict['a_sr']]
        cmap = plt.cm.Paired
        
        count = Counter(res)
        mc = dict(count.most_common())
        mc = {k: v for k, v in sorted(mc.items(), key=lambda item: item[1])}
        colors = cmap(np.linspace(0., 0.8, len(mc)))
        num = len(res)
        print(num)
        print(mc)
        labels = []
        sizes = []
        exp = np.zeros(len(mc))
        for key, item in mc.items():
            labels += [key]
            sizes += [item/num]
        exp[np.argmax(sizes)] = 0.01

        trim = int(3)
        labels = labels[1:trim] + labels[:1] + labels[trim:]
        sizes = sizes[1:trim] + sizes[:1] + sizes[trim:]

        fig, ax = plt.subplots(1, 1, figsize=(12, 9), constrained_layout=True, facecolor='w')
        ax.grid(False)
        pie_wedge_collection = ax.pie(sizes, explode=exp, labels=labels, autopct='%1.2g%%', colors=colors, 
                                      shadow=True, startangle=90, labeldistance=1.05, textprops=dict(size=15, color='k'))
        for pie_wedge in pie_wedge_collection[0]:
            pie_wedge.set_edgecolor('gray')
        #patches, _ = ax.pie(sizes, explode=exp, shadow=True, startangle=90)
        #lbls = [f'{i}:\n{100*j:1.2g} %' for i,j in zip(labels, sizes)]
        #ax.set_ylabel('Fraction')
        #ax.set_title(model, fontsize=20)
        ax.tick_params(axis='x', labelrotation=45)
        #ax = self._axtp(ax, fs=12, c='k')
        #plt.legend(patches, lbls, loc='upper right', bbox_to_anchor=(-0.1, 1.), fontsize=15)
        plt.savefig(savefig, transparent=False)
        #plt.show()

    def plot_tree_count(self, res:list, model:str='Model', savefig:str='Figs/tree.pdf'):

        from collections import Counter
        import squarify
        
        #all_analysis = [item for _, subdict in res.items() for item in subdict['a_sr']]
        cmap = plt.cm.Paired
        
        count = Counter(res)
        mc = dict(count.most_common())
        mc = {k: v for k, v in sorted(mc.items(), key=lambda item: item[1])}
        colors = cmap(np.linspace(0., 0.8, len(mc)))
        num = len(res)
        labels = []
        sizes = []
        exp = np.zeros(len(mc))
        for key, item in mc.items():
            labels += [key]
            sizes += [item/num]
        exp[np.argmax(sizes)] = 0.01
        
        fig, ax = plt.subplots(1, 1, figsize=(12, 9), constrained_layout=True, facecolor='w')
        ax.grid(False)
        squarify.plot(sizes=sizes, label=labels, color=colors, alpha=0.7, ax=ax)

        #patches, _ = ax.pie(sizes, explode=exp, shadow=True, startangle=90)
        #lbls = [f'{i}:\n{100*j:1.2g} %' for i,j in zip(labels, sizes)]
        #ax.set_ylabel('Fraction')
        #ax.set_title(model, fontsize=20)
        #ax.tick_params(axis='x', labelrotation=45)
        #ax = self._axtp(ax, fs=12, c='k')
        #plt.legend(patches, lbls, loc='upper right', bbox_to_anchor=(-0.1, 1.), fontsize=15)
        plt.savefig(savefig, transparent=False)
        #plt.show()

    def plot_bar(self, df: pd.DataFrame, model: str, target:tuple, num_sort:bool=True, 
                 lab:str='', ylog:bool=False, savefig:str='Figs/'):

        fig, ax = plt.subplots(figsize=(7, 6))
        #c = plt.cm.tab10(np.linspace(0, 1, 10))
    
        data = df[target].value_counts()/len(df[target])
        #print(data)
        if num_sort:
            data.sort_index(ascending=True, inplace=True)
        c = ['darkgreen','darkcyan','darkblue']
        data.plot.bar(ax=ax, rot=0, color=c)
        labs = [f'{item}%' for item in np.round(ax.containers[0].datavalues*100, 1)]
        ax.bar_label(ax.containers[0], labels=labs, padding=6.0, label_type='edge', rotation=0, fontsize=12)

        if ylog:
            ax.set_yscale('log')
            ax.set_ylim(ymin=5e-4, ymax=2)
        else:
            ax.set_ylim(ymin=0, ymax=1.2*data.max())
        
        ax.set_ylabel(r'Fraction of Model Points', fontsize=15)
        ax.set_xlabel(f'{lab}', fontsize=15)
        #ax.set_title(f'pMSSM: {model.upper()}', fontsize=15, pad=10)
    
        #l, r = ax.set_xlim(left=int(data.index.min()-1), right=int(data.index.max()+1), emit=False, auto=True)
        #print(l, r)

        ax = self._axtp(ax, fs=15, c='k')

        plt.tight_layout()
        plt.savefig(f'{savefig}{model}_hist.pdf')
        plt.close()




        '''
        def plot_2plain(self, x, y, z_dict_list, axis_lables, bar_lab, vmm, 
                    limits=None, cmap="Purples_r", legfs=20, savefig=False):

        levels=[1.]
        numplot = len(z_dict_list)
        fsize = 16
  
        if cmap is None: 
            figsize = (12, 9)
        else:
            figsize = (13, 9)
        fig, axis = plt.subplots(1, 1, figsize=figsize, sharey=True, constrained_layout=True, facecolor='w')
        k = 0
        fs = 30
        axis.grid(False)
        axis.legend(loc=2, fontsize=legfs, title=r'Exclusion Limits ($r = 1$)', title_fontsize=legfs)
        for z_dict in z_dict_list:
            if numplot > 1:
                ax = axis[k]
            else:
                ax = axis
            if isinstance(z_dict, np.ndarray):
                if z_dict.ndim == 2:
                    z_dict = self.format_arrays(z_dict, titles=['Combined'])

            if not isinstance(z_dict, dict):
                print("Z data not in dictionary format")
                print("{'0':{'Z', [2D numpy array], 'label': 'analysis label' }, '1': {...}, ...}")
                return False

            z = z_dict['0']['Z']

            # Masked array for contour plot
            #zed = ma.masked_less(z, 0) 
            # interpolate data with masked values (less than zero)
            X, Y, Z = self.interpolate_plain(x, y, z, self.dm, min_val=0.0, max_val=None)

        
            #color=iter(cm.tab20(np.linspace(0,1,20)))
            color=iter(cm.Set1(np.linspace(0,1,9)))
            # plot 2D interpolated data
            #pc = ax.pcolormesh(X, Y , Z, norm=colors.PowerNorm(gamma=0.5, vmin=vmm[0], vmax=vmm[1]), cmap=cmap, shading='auto')
            if cmap is not None:
                pc = ax.pcolormesh(X, Y , Z, vmin=vmm[0], vmax=vmm[1], cmap=cmap, shading='auto')
            # get combined countour

            c_list = []
            for i, item in enumerate(z_dict):
                az = z_dict[item]['Z']
                if i == 0:
                    clr = 'k'
                else:
                    clr = next(color).reshape(-1,4)
                #ax, c_temp = self.counour_at_level(ax, x, y, az, levels=levels, clr=clr, label=z_dict[item]['label'])
                #c_temp = ax.contour(x, y , az.T, levels=levels, linestyles='solid', linewidths=4, colors=clr, label=z_dict[item]['label'])
                print(z_dict[item]['label'])
                c_temp = ax.contour(x, y , az.T, levels=levels, linestyles='solid', linewidths=4, colors=clr)

                c_list.append(c_temp)
                c_list[i].collections[i].set_label(z_dict[item]['label'])

            
            if limits is not None:
                ax.set_xlim(*limits[0])
                ax.set_ylim(*limits[1])
            
            ax.set_title(bar_lab[k], fontsize=fs, loc='left', pad=20)
            
            k += 1

            ax = self._axtp(ax, fs=legfs, c='k')
    
        ticks = [0, 0.5, 1, 1.5, 2, 2.5, 3]
        if numplot > 1:
            if cmap is not None:
                cbar = fig.colorbar(pc, orientation="vertical", ax=axis.ravel().tolist(), ticks=ticks)
            axis[0].set_xlabel(axis_lables[0], loc='right', fontsize=fs)
            axis[1].set_xlabel(axis_lables[0], loc='right', fontsize=fs)
            axis[0].set_ylabel(axis_lables[1], loc='top', fontsize=fs)
        else:
            if cmap is not None:
                cbar = fig.colorbar(pc, orientation="vertical", ax=axis, ticks=ticks)
            axis.set_xlabel(axis_lables[0], loc='right', fontsize=fs)
            axis.set_ylabel(axis_lables[1], loc='top', fontsize=fs)
        if cmap is not None:
            cbar.set_label(r'r = $\sigma_{signal}/\sigma_{UL}$', fontsize=fs)
            cbar.ax = self._axtp(cbar.ax, fs=legfs, c='k')

        

        if savefig:
            plt.savefig(savefig, transparent=False)
        
        #fig.subplots_adjust(hspace=0)
        #plt.tight_layout()
        '''