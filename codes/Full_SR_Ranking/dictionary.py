import json
from pathlib import Path

dict_path = f'{Path(__file__).parent.resolve()}/Data/Dicts/'

def get_trans()->dict:
    #trans = from_json(f'{dict_path}mod_dict.json')
    analyses = get_analyses()
    trans = combine_json_from_analyses(analyses)
    return trans

def get_analyses() -> str:
    current = Path(__file__).parent.resolve()
    analysis_file = f'{current}/Data/S_8_13TeV/analysis_regions/ar_8_13TeV.txt'
    with open(analysis_file) as f:
        return f.read().splitlines()

def label_replace(lab:str)->str:
    rep = [(':', '-'), (' ', ''), (',', '-')]
    for item in rep:
        lab = lab.replace(*item)
    return lab

def to_json(dct, fname) -> None:
    with open(f'{dict_path}{fname}.json', "w") as f:
        json.dump(dct, f)

def from_json(path)->dict:
    with open(path, "r") as f:
        dat = json.load(f)
    return dat

def combine_json_from_analyses(analyses)->dict:
    dict_list = {}
    for item in analyses:
        fn = f'{dict_path}{item}.json'
        if Path(fn).exists():
            dict_list |= from_json(fn) 
    return dict_list


def save_analyses(all_dict)->None:

    for key, item in all_dict.items():
        to_json(item, key)

def spit_analyses(trans, corr, analyses)-> dict:
    
    all_dict = {key: {} for key in analyses}
    sumsr = 0
    for key, item in trans.items():
        anid = key.split(":")[0]
        if anid in all_dict:
            rep = label_replace(item)
            all_dict[anid][key] = rep
            sumsr += 1

    print(f"Number of SR's:\n{sumsr}")
    print(f"Percentage of Corelations found:\n{100*sumsr/len(corr):.2f}%")

    return all_dict

def track_sr_names(all_sr_dct:dict, corr:list)->None:
    all_sr = []
    in_lst = []
    no_lst = []
    #strip analyses from all_dict
    for _, sr in all_sr_dct.items():
        all_sr.append(sr)
    #match analyses to correlations
    for item in corr:
        if item in all_sr:
            in_lst.append(item)
        else:
            no_lst.append(item)
    
    
    print(f"Number Found:\n{len(in_lst)}")
    for item in in_lst: print(item)
    print(f"Number Not Found:\n{len(no_lst)}")
    for item in no_lst: print(item)

def write_analyses_from_SModelS_MA5(corr, analyses)->None:
    #write single analysis jsons from SModelS_MA5.json file
    trans = from_json(f'{dict_path}SModelS_MA5.json')
    all_dict = spit_analyses(trans, corr, analyses)
    save_analyses(all_dict)

def main():
    import pandas as pd
    corr = pd.read_csv('Full_SR_Ranking/Data/S_8_13TeV/correlations/combined_correlation_matrix1.txt').keys()
    analyses = get_analyses()
    all_new = combine_json_from_analyses(analyses)
    track_sr_names(all_new, corr)

if __name__ == '__main__':

    main()