#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
from __future__ import print_function
from pathlib import Path
from typing import Union
import pandas as pd
import pyslha

from smodels.tools.xsecComputer import XSecComputer
from smodels.tools.physicsUnits import TeV, pb

class SlhaUtils():
    """
    Slha file handling utilities
    """

    def __init__ (self, order: int=2, temp_slha_path: str='') -> None:
        self.order = (order, self.order_label(order))
        self.slha_path = temp_slha_path
        self.xsc = XSecComputer(maxOrder=order, nevents=50000, pythiaVersion=8)

    @staticmethod
    def order_label(order: int) -> str:
        """return int from SMS order flags"""
        order_level = {0: 'LO', 1: 'NLO', 2: 'NLL', 3: 'NNLL'}
        return order_level[order]

    @staticmethod
    def load_template(filepath: str) -> Union[str,  None]:
        """load slha file"""
        try:
            with open(filepath) as o_f:
                return o_f.read()
        except OSError:
            print("Could not open/read file:")
        return None

    @staticmethod
    def save_slha(filepath, txtfile:str) -> None:
        """Save slha to template location"""
        try:
            with open(filepath, 'w') as o_f:
                o_f.write(txtfile)
        except OSError:
            print("Could not write file:")

    @staticmethod
    def save_to_json(d_f, fpath: str) -> None:
        """save data frame to json fomat"""
        join = False
        if Path(fpath).exists():
            d_f0 = pd.read_json(fpath, orient='index')
            join = True
        if join:
            d_f = pd.concat([d_f0, d_f], ignore_index=True)
            d_f.drop_duplicates(subset=['data_ID'], inplace=True, ignore_index=True)
        d_f.to_json(fpath, orient='index')

    @staticmethod
    def check_for_directory(dpath: str) -> None:
        """Check if dir exists, if not create one"""
        Path(dpath).mkdir(parents=True, exist_ok=True)

    @staticmethod
    def add_xsec_to_slha(text_file:str, x_secs:list) -> str:
        """ adds list of x sections to end of string """
        if text_file[-1] == '\n':
            text_file += '#'
        for xsec in x_secs:
            text_file += ( xsec + "\n")
        return text_file
    
    @staticmethod
    def get_xsec_from_str(slhafile)-> float:
        ret = []
        f = pyslha.readSLHAFile(slhafile)
        for production in f.xsections:
            process = f.xsections.get ( production )
            for pxsec in process.xsecs:
                wlabel = str( int ( pxsec.sqrts / 1000) ) + ' TeV'
                ret.append({'sqrts': pxsec.sqrts/1000. * TeV,
                            'order': pxsec.qcd_order,
                            'value': pxsec.value * pb,
                            'label': wlabel,
                            'pid': production[2:]
                            })
                
        return ret
