#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
from __future__ import print_function
from pathlib import Path
import pandas as pd
import numpy as np
from itertools import product
from smodels.tools.physicsUnits import GeV, TeV
from .data_handler import SlhaUtils
from copy import copy
from ..tpred_combinations import CombineSignalRegions
from smodels.experiment.exceptions import SModelSExperimentError

class TopologyScan(SlhaUtils):

    def __init__ (self, slha_dir: str, order: int=2, temp_slha_path: str='', mass_ranges: list=None, dm:float=25.) -> None:
        SlhaUtils.__init__(self, order=order, temp_slha_path=temp_slha_path)
        self._tmpl8_dict = self.get_template_locs(slha_dir)
        self.mass_values = self.__set_mass_array(mass_ranges, int(dm))
        self.cachedObjs = {}
        

    def __set_mass_array(self, mass_ranges:list, dm:float) -> np.ndarray:
        temp = []
        for mr in mass_ranges:
            temp.append(self.set_ranges(*mr, dm))
        return np.array(temp, dtype=object)

    @staticmethod
    def get_template_locs(templatePath:str) -> dict:
        p = list(Path(templatePath).iterdir())
        paths = [str(x) for x in p if x.is_file()]
        ret = {}
        for item in paths:
            ret[item.split("/")[-1].split(".")[0]] = item
        return(ret)

    @staticmethod
    def replace_mass(text_file:str, item_name:str, new_item:str) -> str:
        """ replace term in string: specifically mass of particle"""
        text_file = text_file.replace(item_name, new_item)
        return text_file

    @staticmethod
    def set_ranges(start:float, stop:float, step:float) -> np.ndarray:
        return np.arange(start, (stop + step), step)
    
    @staticmethod
    def set_mass_list(masses:list) -> list:
        ret = [mass*GeV for mass in masses]
        return [ret, ret]

    @staticmethod
    def data_id(top:str, mass_l:list, mass_v:list):
        name = top
        for mass, val in zip(mass_l, mass_v):
            name += '_{}_{}'.format(mass, int(val))
        return(name)

    @property
    def default_file_location(self, topology:str) -> str:
        return f'/Model_Data/Topologies/{topology}/{topology}_Xsec.pkl'

    @property
    def get_mass_labels(self) -> tuple:
        return tuple([f'M{i}' for i in range(len(self.mass_values))])

    @property
    def get_mass_points(self) -> list:
        if 'mass_points' not in self.cachedObjs:
            self.cachedObjs['mass_points'] = list(product(*self.mass_values))
        return self.cachedObjs['mass_points']

    @property
    def get_grid_locations(self) -> list:
        if 'grid_locs' not in self.cachedObjs:
            prod_list = [range(len(item)) for item in self.mass_values]
            self.cachedObjs['grid_locs'] = list(product(*prod_list))
        return self.cachedObjs['grid_locs']
    
    @property
    def get_grid_dims(self) -> tuple:
        return tuple([len(item) for item in self.mass_values])

    @property
    def empty_r_array(self) -> np.ndarray:
        return np.ones(self.get_grid_dims) * -np.inf
    

    def get_template(self, topology) ->str():
        return self.load_template(self._tmpl8_dict[topology])
    
    def get_mass_from_grid(self, grip_point) -> list:
        return [m[p] for m, p in zip(self.mass_values, grip_point)]

    def get_grid_from_mass(self, mass_point) -> list:
        mass_point = tuple(mass_point)
        if mass_point in self.get_mass_points:
            index = self.get_mass_points.index(tuple(mass_point))
            return tuple(self.get_grid_locations[index])
        else:
            return tuple()
    
    def edit_slha(self, tmplt:str, mass_label:list, mass_value:list)->str:
        for mass, val in zip(mass_label, mass_value):
            tmplt  = self.replace_mass(tmplt, mass, str(val))
        return tmplt

    def xsec_dict(self, mass_value:list, topology:str='T1', roots:list=[8])->dict:
        ret = {}
        ret['data_ID'] = self.data_id(topology, self.get_mass_labels, mass_value)
        ret['sqrt_s'] = roots
        ret['mass_labels'] = self.get_mass_labels
        ret['mass_values'] = tuple(mass_value)
        return ret

    def xsec_at_point(self, mass_value:list, topology:str='T1', roots:list=[8]) -> dict:
        ret = self.xsec_dict(mass_value, topology, roots)
        #tmplt = self.load_template(self._tmpl8_dict[topology])
        tmplt = self.get_template(topology)
        self.check_for_directory(self.slha_path)
        sname = self.slha_path + f'{topology}_temp.slha'
        tmplt = self.edit_slha(tmplt, ret['mass_labels'], mass_value)
        self.save_slha(sname, tmplt)

        x_sec = []
        for rts in roots:
            x_sec += self.xsc.compute (rts * TeV,  sname,  lhefile=None, unlink=True,
                                      loFromSlha=False, pythiacard=None, ssmultipliers=None)
        if x_sec:
            ret['X-sec'] =  [self.xsc.xsecToBlock(xs) for xs in x_sec]

        return ret

    def get_mgrid_x_secs(self, ssr_object, top:str, fpath:str, roots:list=8):

        #check for existing file
        update = Path(fpath).exists()

        dflables = []
        if update:
            print('Data Frame Found')
            print('Checking if new data is required')
            dflables = pd.read_json(fpath, orient='index')['data_ID'].to_list()

        master = []
        mpoints = self.get_mass_points
        mlabels = self.get_mass_labels
        save_click = 0

        for mass_p in mpoints:
            # get unique mass point & topology label
            #name = self.set_mass_label(top, mlabels, mass_p)
            name = self.data_id(top, mlabels, mass_p)
            # continuation conditional for optimization
            continue_ = True
            #check if data exists in previous df
            if name in dflables:
                continue_ = False
            if continue_:
                # put mass values into the SModels format
                masses = self.set_mass_list(mass_p)
                # check if there is an efficiency map / upperlimit in SModels for given mass
                if ssr_object.efficiency_bool(masses):
                    print(name)
                    master += [self.xsec_at_point([*mass_p], topology=top, roots=roots)]
                    save_click += 1
            # save drop out and reset master every 50 X-sec calculations
            if not save_click % 50:
                if master:
                    print("Saving")
                    self.save_to_json(pd.DataFrame(master), fpath)
                    master = []
        # final save if there are item in master
        if master:
            self.save_to_json(pd.DataFrame(master), fpath)

    @staticmethod       
    def list_sets(anas_and_SRs: dict):
        ana = []
        sigres = []
        for gp in anas_and_SRs:
            keys = [*anas_and_SRs[gp]['a_sr']]
            ana += keys
            for key in keys:
                sigres += anas_and_SRs[gp]['a_sr'][key]
        
        analysis_set = set([x for x in ana if ana.count(x) > 1])
        sgnl_rgn_set = set([x for x in sigres if sigres.count(x) > 1])

        return analysis_set, sgnl_rgn_set

    @staticmethod
    def single_analysis(anas_and_SRs: dict, keep: list) -> dict:
        """
        Re-structure analysis and signal region dictionary to only include analyses from list 'keep'
        i.e. keep = ['CMS-SUS-13-012'] -> ret_dict = anas_and_SRs minus all analyses != 'CMS-SUS-13-012'
        """
        ret_dict = {}
        for gp in anas_and_SRs:
            a_sr = {}
            keep_in = False
            for keeper in anas_and_SRs[gp]['a_sr']:
                if keeper in keep:
                    keep_in = True
                    idx = keep.index(keeper)
                    a_sr[keeper] = anas_and_SRs[gp]['a_sr'][keep[idx]]
            if keep_in:
                ret_dict[gp] = copy(anas_and_SRs[gp])
                ret_dict[gp]['a_sr'] = a_sr
        return ret_dict

    @staticmethod   
    def single_analysis_and_sr(anas_and_sr:dict, analysis:str, signal_region:str) -> dict:
        """
        Re-structure analysis and signal region dictionary to only include one analyses signal region
        """
        
        ret_dict = {}
        for gp in anas_and_sr:
            for an in anas_and_sr[gp]["A_SR"]:
                if an == analysis:
                    for sr in anas_and_sr[gp]["A_SR"][analysis]:
                        if sr == signal_region:
                            ret_dict[gp] = copy(anas_and_sr[gp])
                            ret_dict[gp]["A_SR"] = {analysis : [signal_region]}
        return ret_dict

    def get_r_values(self, dbase: object, df: pd.DataFrame, anas_and_SRs: dict, top: str, 
                     combine:bool=True, bestSet:bool=False) -> dict:
        """
        
        """
        #from ..combined_result import CombineSignalRegions
        csr = CombineSignalRegions()
        ret = copy(anas_and_SRs)

        print('Calculating r-values')
        #for gp in anas_and_SRs:
        for key, item in anas_and_SRs.items():
            index = item['df_index']
            slha_fpath = self.create_slha_from_dict(df.loc[index], topology=top, save_temp=True)
            try :
                robs, rexp = csr.r_value(slha_fpath, item['a_sr'], database=dbase, 
                                     combine=combine, bestSet=bestSet, verbose=False)

            except SModelSExperimentError:
                robs, rexp = np.nan, np.nan
                print("Somthing went wrong")

            ret[key]['r_obs'] = robs
            ret[key]['r_exp'] = rexp

        return ret

    def get_chi_values(self, dbase: object, df: pd.DataFrame, anas_and_SRs: dict, top: str, 
                       combine:bool=True, bestSet:bool=False, am:bool=False) -> dict:
        """
        
        """
        csr = CombineSignalRegions()
        ret = copy(anas_and_SRs)

        print('Calculating chi-values')
        #for gp in anas_and_SRs:
        for key, item in anas_and_SRs.items():
            index = item['df_index']
            slha_fpath = self.create_slha_from_dict(df.loc[index], topology=top, save_temp=True)
            try :
                res = csr.chi_value(slha_fpath, item['a_sr'], database=dbase, expected=False, combine=combine, 
                                    bestSet=bestSet, verbose=False, anomaly_mode=am, r_value=True)

            except SModelSExperimentError:
                res = 3*[np.nan]
                print("Somthing went wrong")

            ret[key]['chi'] = res[0]
            ret[key]['lsm'] = res[1]
            ret[key]['lmax'] = res[2]
            ret[key]['rval'] = res[3]

        return ret

    def fill_r_gird(self, r_dict:dict, lab):
        ret = self.empty_r_array
        for gp in r_dict:
            ret[gp] = r_dict[gp][lab]
        return ret

    def create_slha_from_dict(self, xs_dict, topology='T1', save_temp=True, ftag='temp'):

        #load templat
        tmplt = self.load_template(self._tmpl8_dict[topology])
        #replace mass values
        for mass, val in zip(xs_dict['mass_labels'], xs_dict['mass_values']):
            tmplt  = self.replace_mass(tmplt, mass, str(val))
        # add X-sec
        tmplt = self.add_xsec_to_slha(tmplt, xs_dict['X-sec'])

        if save_temp:
            sname = f'{self.slha_path}{topology}_{ftag}.slha'
            self.save_slha(sname, tmplt)
            return(sname)
        else:
            return tmplt




