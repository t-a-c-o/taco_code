#!/usr/bin/env python
"""
############################################################################################
# Script written to find optimum signal regions, as a continuation of the LH2019 project   #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'            #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger, S. Williamson and J. Yellen  #
############################################################################################
"""
from __future__ import print_function
from pathlib import Path
import pandas as pd
import numpy as np
from itertools import product
from smodels.tools.physicsUnits import GeV, TeV
from .data_handler import SlhaUtils
from copy import copy

class FullModelScan(SlhaUtils):

    def __init__ (self, slha_dir: str, order: int=2, temp_slha_path: str='') -> None:
        SlhaUtils.__init__(self, order=order, temp_slha_path=temp_slha_path)
        self._tmpl8_dict = self.get_template_locs(slha_dir)

    @staticmethod
    def get_template_locs(slha_path: str) -> dict:
        """list the slha files in directory"""
        ret = {}
        ret['directory'] = slha_path
        p_l = list(Path(slha_path).iterdir())
        temp =  [str(x).split('/')[-1] for x in p_l if x.is_file()]
        for i, item in enumerate(sorted(temp)):
            ret[i] = item
        return ret

    def absolute_path(self, file_number:str) -> str:
        """ return path to slha file """
        return self._tmpl8_dict['directory'] + self._tmpl8_dict[file_number]

    def get_all(self, save_path:str, roots: list=[13]) -> None:
        """ get all the x sections for files listed in self._tmpl8_dict"""
        # initalise the data variables
        master = []
        data_ids = []
        save_click = 0
        # initiate save path and ensure path is available
        self.check_for_directory(save_path)
        fpath = save_path + f'{self.order[1]}_Xsecs.json'
        # check for previous save
        if Path(fpath).exists():
            print('Data Frame Found')
            print('Checking if new data is required')
            d_f = pd.read_json(fpath, orient='index')
            data_ids = d_f['data_ID'].to_list()
        # check for previous save
 
        for i in range(len(self._tmpl8_dict.keys()) - 1):
            if self._tmpl8_dict[i] not in data_ids:
                print(f'Processing Num: {i} -> {self._tmpl8_dict[i]}')
                master += [self.get_x_sections(i, roots=roots, xsec_to_temp=False)]
                save_click += 1
            # save drop out and reset master every 50 X-sec calculations
            if not save_click % 50:
                if master:
                    print("Saving")
                    self.save_to_json(pd.DataFrame(master), fpath)
                    master = []
        # final save if there are item in master
        if master:
            self.save_to_json(pd.DataFrame(master), fpath)

    def add_new_xsec(self, save_path:str, roots: list=[13]) -> None:
        # initalise the data variables
        self.check_for_directory(save_path)
        master = []
        lpath = save_path + f'{self.order[1]}_Xsecs.json'
        spath = save_path + f'{self.order[1]}_Xsecs_add.json'
        d_f = pd.read_json(lpath, orient='index')
        save_click = 0
        for i, xsecs in enumerate(d_f['X-sec'].to_list()):
            missing = self.find_missing(xsecs, roots)
            if missing:
                temp = self.get_x_sections(d_f.iloc[i]['file_number'], roots=missing, xsec_to_temp=False)
                temp['X-sec'] += d_f.iloc[i]['X-sec']
                master += [temp]
                save_click += 1
                if not save_click % 50:
                    print(f'saving, n={i}')
                    self.save_to_json(pd.DataFrame(master), spath)
                    master = []
        self.save_to_json(pd.DataFrame(master), spath)

    @staticmethod
    def find_missing(x_secs: list, roots: list, pow: float=1e3) ->list:
        res = []
        labs = []
        for rts in roots:
            labs += ['{:.2E}'.format(rts*pow)]
        for x_s in x_secs:
            line = x_s.split(' ')
            for rs, lab in zip(roots, labs):
                if lab not in line:
                    res += [rs]
        return list(set(res))

    def get_x_sections(self, file_number: int, roots: list=[13], xsec_to_temp: bool=False) -> dict:
        """ get x section for a particular slha file """

        ret = {}
        ret['file_number'] = file_number
        ret['data_ID'] = self._tmpl8_dict[file_number]
        ret['X-sec'] = []
        file_path = self.absolute_path(file_number)
        tmplt = self.load_template(file_path)
        self.check_for_directory(self.slha_path)
        sname = self.slha_path + 'temp.slha'
        self.save_slha(sname, tmplt)
        x_sec = []
        for rts in roots:
            x_sec += self.xsc.compute (rts * TeV,  sname,  lhefile=None, unlink=True,
                                      loFromSlha=False, pythiacard=None, ssmultipliers=None)
        if x_sec:
            ret['X-sec'] +=  [self.xsc.xsecToBlock(xs) for xs in x_sec]
        if xsec_to_temp:
            self.create_slha_from_dict(ret, save_temp=True)
        return ret

    def create_slha_from_dict(self, xs_dict:dict, save_temp:bool=True, temp_name:str='temp.slha'):
        """ tke x sec data and coreponding slha file, join them and save"""
        #load templat
        file_path = self.absolute_path(xs_dict['file_number'])
        #replace mass values
        tmplt = self.load_template(file_path)
        #print(file_path)
        # add X-sec
        tmplt = self.add_xsec_to_slha(tmplt, xs_dict['X-sec'])

        if save_temp:
            sname = self.slha_path + temp_name
            self.save_slha(sname, tmplt)
            return sname
        return tmplt