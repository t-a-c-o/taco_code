#!/usr/bin/env python3

"""
   :synopsis: executable that builds the dictionary between MA5 signal region names
              and SModelS signal region names
"""

from smodels.experiment.databaseObj import Database
import sys, time, os
import glob

green,red,reset="","",""
try:
    import colorama
    green = colorama.Fore.GREEN
    red = colorama.Fore.RED
    reset = colorama.Fore.RESET
except ImportError as e:
    pass

def getSModelS():
    ## instantiate a database object
    dbpath = "official+fastlim+superseded+nonaggregated"
    #dbpath = "https://smodels.github.io/database/jamie"
    # dbpath = "/home/walten/git/branches/smodels-database"
    # dbpath = '/Users/jamieyellen/Desktop/TACO/Full_Model/Model_Data/DataBase/official220alpha1.pcl'
    database = Database( dbpath )

    anaids = [ "all" ]
    # anaids = ['ATLAS-SUSY-2018-06', 'ATLAS-SUSY-2016-07', 'ATLAS-SUSY-2018-04', 'CMS-SUS-19-006', 'ATLAS-SUSY-2018-31', 'ATLAS-SUSY-2013-21', 'ATLAS-SUSY-2013-02', 'ATLAS-SUSY-2013-05', 'CMS-SUS-16-033', 'CMS-SUS-16-039', 'CMS-SUS-13-011', 'ATLAS-SUSY-2018-32', 'ATLAS-SUSY-2013-04', 'CMS-SUS-13-012', 'ATLAS-SUSY-2015-06', 'CMS-SUS-17-001', 'ATLAS-SUSY-2013-11', 'ATLAS-SUSY-2019-08' ]
    # anaids = [ "ATLAS-SUSY-2018-32" ]
    # get all the efficiency map results
    expResults = database.getExpResults( analysisIDs = anaids, 
                                         dataTypes = [ "efficiencyMap" ],
                                         useNonValidated = True )

    ret, numbers = {}, {}
    ## iterate through the list
    for expRes in expResults:
        analysisId = expRes.globalInfo.id
        ## iterate through the "datasets" ( = signal regions )
        for ds in expRes.datasets:
            smId = f"{analysisId}:{ds.dataInfo.dataId}" 
            #print ( smId )
            dI = ds.dataInfo
            #print ( f"nobs: {dI.observedN} expBG: {dI.expectedBG}+-{dI.bgError}" )
            if not analysisId in ret:
                ret[analysisId]=[]
            ret[analysisId].append ( ds.dataInfo.dataId )
            numbers[ smId ] = { "obsN": dI.observedN, "expBG": dI.expectedBG,
                                "bgErr": dI.bgError }
    return ret, numbers

def cleanUp ( inp ):
    old = inp
    inp = inp.replace("<","").replace("nobs","").replace(">","").replace("/","")
    inp = inp.replace("deltanb_syst","")
    inp = inp.replace("deltanb_stat","").replace("deltanb","").replace("nb","")
    inp = inp.strip()
    try:
        inp = float(inp)
    except Exception as e:
        print ( "[build_dictionary] exception", e, "in", old, "inp is", inp )
    return inp

def getMA5():
    files = glob.glob ( "ma5stuff/*info" )
    ret, numbers = {}, {}
    for f in files[:]:
        h = open ( f, "rt" )
        ma5anaid = f.replace("ma5stuff/","").replace(".info","")
        anaid = ma5anaid.upper().replace("_","-")
        lines = h.readlines()
        h.close()
        for i,line in enumerate(lines):
            if not "region" in line or not "signal" in line:
                continue
            if not "type" in line:
                continue
            if "-  signal regions" in line:
                continue
            if "results are given after combination" in line:
                continue
            line = line.strip()
            p = line.find("id")
            p2 = line.rfind(">")
            token = line[p+4:p2-1]
            idfer = "%s:%s" % ( ma5anaid, token )
            # print ( "anaid %s ''%s:%s''" % ( anaid, ma5anaid, token ) )
            if not ma5anaid in ret:
                ret[ma5anaid]=[]
            ret[ma5anaid].append ( token )
            nobs = cleanUp ( lines[i+1] )
            nbg = cleanUp ( lines[i+2] )
            bgexp = cleanUp ( lines[i+3] )
            numbers[idfer]={ "obsN": nobs, "expBG": nbg, "bgErr": bgexp }
            # ret.append ( idfer )
        cppfile = f.replace("info","cpp")
        if os.path.exists( cppfile ):
            # we can extract SR names from the cpp file
            #print ( "ret", ret[ma5anaid])
            #print ( "numbers", numbers[idfer] )
            #print ( "f", cppfile )
            h = open ( cppfile, "rt" )
            lines = h.readlines()
            for line in lines:
                if not "AddRegionSelection" in line:
                    continue
                p1 = line.find('("')
                p2 = line.find('")')
                tanaid = line[p1+2:p2]
                if not tanaid in ret[ma5anaid]:
                    ret[ma5anaid].append ( tanaid )
    return ret, numbers

def toMA5anaId ( smid ):
    """ convert smodels ana id to ma5 anaid """
    if smid.find(":")>0:
        tokens=smid.split(":")
        tokens[0]=tokens[0].lower()
        tokens[0]=tokens[0].replace("-","_")
        return tokens[0]+":"+tokens[1]
    smid = smid.lower()
    smid = smid.replace("-eff","")
    smid = smid.replace("-ma5","")
    # smid = smid.replace("-agg","") ## ma5 cannot have these
    smid = smid.replace("-","_")
    return smid

def toSModelSanaId ( smid ):
    """ convert ma5 ana id to smodels anaid """
    smid = smid.upper()
    smid = smid.replace("_","-")
    return smid
            
def findRegion ( srid, ma5regions, ma5id ):
    """ find the MA5 signal region for the SModelS signal region,
    match by name 
    :param srid: SModelS signal region name
    :param ma5regions: container of MA5 SR names
    :param ma5id: name of MA5 analysis, so we can perform special hacks
                  for specific analyses
    :returns: MA5 signal region name, or False if nothing was found
    """
    # print ( f"[build] search for ''{srid}'' in ''{', '.join(ma5regions)[:30]}...''" )
    if ma5id == "atlas_susy_2018_32":
        srid = srid.replace("-","_")
        srid = srid.replace("=[","_")
        srid = srid.replace(")","")
        srid = srid.replace(",","_")
        srid = srid.replace("mt2","MT2")
    for ma5r in ma5regions:
        srid2= srid
        # try without SR
        if not ma5r.startswith("SR"):
            srid2 = srid.replace("SR","")
        # print ( f"special rules for {ma5id[11:]} ''{srid2}'' ''{ma5r}'' {srid2 in ma5r}" )
        if srid2 == ma5r:
            return ma5r
        if ma5r.startswith( srid2+"_" ):
            # SR171 matches SR171_xxx_yyy
            # print ( f">>> {ma5r} starts with {srid2}, should i allow the match?" )
            return ma5r
        #if srid2.startswith( ma5r ):
            # SR171 matches SR171_xxx_yyy
            # print ( f">>> {srid2} starts with {ma5r}, should i allow the match?" )
            # return ma5r
        if srid2 in ma5r:
            # print ( f">>> {srid2} in {ma5r}, should i allow the match?" )
            pass
        srid2 = srid2.replace("_","")
        ma5r2 = ma5r.replace("_","")
        if srid2 in ma5r2:
            return ma5r
        srid2 = srid2.lower()
        ma5r2 = ma5r2.lower()
        if srid2 in ma5r2:
            return ma5r
        srid2 = srid2.replace("mct","ct")
        ma5r2 = ma5r2.replace("mct","ct")
        if srid2 in ma5r2:
            return ma5r
    return False

def findByNumbers ( smId, ma5id, smnumbers, ma5numbers, maxN, verbose ):
    # print ( "[build] need to find nobs", smnumbers[smId] )
    c=0
    smallestN, smallest, smallesk = float("inf"), {}, None
    smsmallest = {}
    smallestSMID=None
    for k,v in ma5numbers.items():
        if not ma5id in k:
            continue
        c+=1
        dObs = abs ( smnumbers[smId]["obsN"] - v["obsN"] )
        dexp = abs ( smnumbers[smId]["expBG"] - v["expBG"] )
        derr = abs ( smnumbers[smId]["bgErr"] - v["bgErr"] )
        nObs = ( smnumbers[smId]["obsN"] + v["obsN"] )
        nexp = abs ( smnumbers[smId]["expBG"] + v["expBG"] )
        nerr = abs ( smnumbers[smId]["bgErr"] + v["bgErr"] )
        if nObs > 0.:
            dObs = dObs / nObs
        if nexp > 0.:
            dexp = dexp / nexp
        if nerr > 0.:
            derr = derr / nerr
        if dObs < 1e-5 and dexp < 1e-5 and derr < 1e-5:
            # print ( "numbers match!!!" )
            return k
        dtotal = dObs + .5*dexp + .25*derr
        ## try to identify closest match
        if dtotal < smallestN:
            smallestN = dtotal
            smallest = v
            smallestk = k
            smsmallest = smnumbers[smId]
            smallestSMID=smId
            
    ## try again but with relaxed requirements
    if smallestN < maxN:
        if verbose>=2:
            print ( f"[build] {green}taking closest match: {smallestk} {smallest} <=> {smallestSMID}:{smsmallest}{reset}" )
        return smallestk
    else:
        print ( f"[build] {red}no matching numbers found, MA5 closest to SModelS was:\n           {smallestk}: d={smallestN:.3f}: {smallest} <=> {smallestSMID}:{smsmallest}{reset}" )
    return False

def match ( ma5t, ma5numbers, smt, smnumbers, verbose = 0 ):
    """ try and match ma5 with smt """
    D = {}
    for ct,(smid,smn) in enumerate(smt.items()):
        if verbose>=2:
            print ( f"[build] checking #{ct}: {smid}" )
        #if ct > 13:
        #    break
        ma5tokens = toMA5anaId(smid).split(":")
        foundMatch=False
        for ma5,ma5regions in ma5t.items():
            if ma5tokens[0] in ma5:
                foundMatch=True
                break
        if not foundMatch:
            print ( f"[build] {red}Did not find {ma5tokens[0]} in ma5{reset}" )
            continue
        ma5id =  toMA5anaId ( smid )
        for srid in smn:
            # print ( smid, srid, "->", ma5id )
            smline = smid + ":" + srid
            if srid in ma5regions:
                ma5line = ma5id + ":" + srid
                D[smline]=ma5line
                D[ma5line]=smline
                # print ( "[build] exact match: %s:%s " % ( smline, ma5line ) )
                continue
            fr = findRegion ( srid, ma5regions, ma5id )
            if fr != False:
                ma5line = ma5id + ":" + fr
                D[smline]=ma5line
                D[ma5line]=smline
                # print ( "[build] close match: %s -> %s " % ( smline, ma5line ) )
                continue
            if verbose>=3:
                print ( "[build] no direct match: %s <-> %s" % ( smline, ma5id ) )
            maxN = .1
            if ma5id == "atlas_susy_2018_32":
                maxN = .001
            fr = findByNumbers(smline, ma5id, smnumbers, ma5numbers, maxN, verbose)
            if fr != False:
                ma5line = fr # ma5id + ":" + fr
                D[smline]=ma5line
                D[ma5line]=smline
                if verbose >=2:
                    print ( "[build] matched by numbers: %s -> %s " % ( smline, ma5line ) )
                continue
            if fr == False:
                print ( "[build] no match: %s <-> %s:%s" % ( smline, ma5id, ",".join ( ma5regions[:2] ) ) )
    return D

def main():
    ma5t, ma5numbers = getMA5()
    smt, sm5numbers = getSModelS()
    verbose = 1
    D = match( ma5t, ma5numbers, smt, sm5numbers, verbose )
    print ( f"writing {len(D)} entries to dictionary.py" )
    f=open("dictionary.py","wt")
    f.write ( f"# {time.asctime()}: {len(D)} entries total\n" )
    f.write ( str(D)+"\n" )
    f.close()

main()
