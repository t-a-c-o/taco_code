import pandas as pd
from scipy import stats
import os
import random




def Selection(srs,frame,acceptancematrix):

    accepted=True
    for sr_1 in srs:
        for sr_2 in srs:
            if sr_1==sr_2:
                continue
            n=int(frame[[sr_1]].sum())+int(frame[[sr_2]].sum())
        
            pair=frame[[sr_1,sr_2]]
            pair=pair.loc[pair[sr_1]>0]
        
            k=int(pair[sr_2].sum())
            if k>100:
         
                continue
            if (n<299+111.55*k) and  (n>1+1.07567*k):
                accepted=False
                print('NON accepted')
                print(sr_1)
                print(sr_2)
                acceptancematrix[sr_1][sr_2]=-1
               
            
        
    
  
    return acceptancematrix
    
def SaveAcceptanceMatrix(acceptancematrix,name):

    acceptancematrix.to_csv(name)
    
    
    return



def createAcceptanceFrame(frame):

    srs=list(frame.columns)
    acceptancematrix = pd.DataFrame(0, index=srs, columns=srs)

    return acceptancematrix,srs



all_events_path='../TACOing/8-TeV_results/frame_events_8TeV_nonzeros_run2and3_all.txt'
name='acceptance_matrix_8TeV.txt'


frame=pd.read_csv(all_events_path)
acceptancematrix,srs=createAcceptanceFrame(frame)

acceptancematrix=Selection(srs,frame,acceptancematrix)
SaveAcceptanceMatrix(acceptancematrix,name)
