#!/usr/bin/env python

############################################################################################
# Script written to produce convex mass hulls, as a continuation of the LH2019 project       #
# 'Determination of Independent Signal Regions in LHC Searches for New Physics'             #
# by A. Buckley, B. Fuks, H. Reyes-González, W. Waltenberger and S. Williamson.             #
############################################################################################

# from __future__ import print_function

#~~~~~~~
#script can be separated up into stand alone bits (denoted by --%-- sections) depending on which information has been produced/files are installed for the user.
#~~~~~~

import re
from numpy import *
import os
import pandas as pd
import urllib.request, urllib.error, urllib.parse
from bs4 import BeautifulSoup
import requests
import shutil
import numpy as np
from scipy.spatial import ConvexHull, convex_hull_plot_2d
import matplotlib.pyplot as plt
import pickle

# #~~~~~~~
# # if smodels is not installed, try e.g. pip3 install --user smodels
# # which fetches the official database from a CERN server automatically. If this does not work, install SModelS database manually.
# # then do
from smodels.experiment.databaseObj import Database
from smodels.tools.physicsUnits import GeV, pb, fb
# # If database is already installed, continue with:

# # Opens SModelS database and writes analysis names to ListofAnalysesSModelS variable.
db = Database("./sophies_world.pcl", force_load="pcl" )
# #~~~~~~~
# #-------%-------#
    #(Section 1): finds common analyses between SmodelS and MA5.
# #-------%-------#

# # Downloads MA5 PAD and extracts analysis names to AnalysesInMA5 variable.

# url = 'https://madanalysis.irmp.ucl.ac.be/wiki/PublicAnalysisDatabase'
# res = requests.get(url)
# html_page = res.content
# soup = BeautifulSoup(html_page, 'html.parser')
# ATLAS = soup.find_all(string=re.compile("ATLAS"))
# CMS = soup.find_all(string=re.compile("CMS"))

# AnalysesInMA5 = []
# for lines in ATLAS:
#     substringATLAS = r"^ATLAS-(\S+)-(\S+)-(\S+)$"
#     if re.search(substringATLAS, lines):
#         AnalysesInMA5.append(lines)
# for lines in CMS:
#     substringCMS = r"^CMS-(\S+)-(\S+)-(\S+)$"
#     if re.search(substringCMS, lines):
#         AnalysesInMA5.append(lines)
# print("Analyses in MA5\n", AnalysesInMA5, "\n")


# resultsSModelS = db.getExpResults ( analysisIDs = ["all"])
# ListofAnalysesSModelS = []
# for result in resultsSModelS:
#          ListofAnalysesSModelS.append((result.globalInfo.id))
# print("Analyses in SModelS\n", ListofAnalysesSModelS, "\n")

# # Finds common analyses between SModelS and MA5 and writes results to Database_Lists.txt

# SharedAnalyses= list(set(ListofAnalysesSModelS) & set(AnalysesInMA5))
# print("Shared Analyses\n", SharedAnalyses)

# results = db.getExpResults ( analysisIDs = SharedAnalyses )

# Database_File = open("Database_Lists.txt","w+")
# Database_File.write("Analyses in MA5\n")
# Database_File.write("\n")
# Database_File.write(format(str(AnalysesInMA5)))
# Database_File.write("\n")
# Database_File.write("\n")
# Database_File.write("Analyses in SModelS\n")
# Database_File.write("\n")
# Database_File.write(format(str(ListofAnalysesSModelS)))
# Database_File.write("\n")
# Database_File.write("\n")
# Database_File.write("Shared Analyses\n")
# Database_File.write("\n")
# Database_File.write(format(str(SharedAnalyses)))
# Database_File.close()


# #-------%-------#
    #(Section 2): Find convex hull of masses for each topology probed in the analyses 'resultsset'.
# #-------%-------#

# #~~~~~~~
# # the below are the common analyses in between SModelS and MA5 as of February 23 2021. If no new updates to MA or SModelS, can comment out section (1) above.
# #~~~~~~~

#resultsset = db.getExpResults (analysisIDs=[, , , , , , , , , , , , , , , ,, ])

###13 tev
#resultsset = db.getExpResults (analysisIDs=['ATLAS-SUSY-2015-06','ATLAS-SUSY-2016-07','ATLAS-SUSY-2018-06','ATLAS-SUSY-2018-04','ATLAS-SUSY-2018-31','ATLAS-SUSY-2018-32','ATLAS-SUSY-2019-08','CMS-SUS-19-006','CMS-SUS-16-033','CMS-SUS-16-039','CMS-SUS-17-001'])

### 8 TeV
#resultsset = db.getExpResults (analysisIDs=['ATLAS-SUSY-2013-21','ATLAS-SUSY-2013-02','ATLAS-SUSY-2013-05','CMS-SUS-13-011','ATLAS-SUSY-2013-04','CMS-SUS-13-012','ATLAS-SUSY-2013-11'])

resultsset = db.getExpResults (analysisIDs=['ATLAS-SUSY-2013-21'])
# resultstestset = db.getExpResults (analysisIDs=['ATLAS-SUSY-2013-21','ATLAS-SUSY-2018-06', 'CMS-SUS-19-006']) # Analysis test sample for quicker test runs.

MassiveDictionary={}

def ObtainMassTuples (txname):
    """ A function that extracts the mass points from a txname object and puts them in tuples. Origdata is an object with mass and luminosity information within the SModelS Sophie's World pcl. """
    for x in range(0,len(origdata)):
        mass1 = float(origdata[x][0][0][0].asNumber(GeV))
        mass2 = float(origdata[x][0][0][1].asNumber(GeV))
        if(len(origdata[0][0][0])==2): # Two body decays
            masstuple = (mass1,mass2)
        elif(len(origdata[0][0][0])==3): # Three body decays
            mass3 = float(origdata[x][0][0][2].asNumber(GeV))
            masstuple = (mass1,mass2,mass3)
        MassiveDictionary[txname.txName].add(masstuple) # Mass information [masstuple] (without unit) added to the set of the relevant topology name [txname.txName] within a dicionary [MassiveDictionary].

for expRes in resultsset:
    for dataset in expRes.datasets: # dataset is the list of topologies in an analysis
        for txname in dataset.txnameList: # loop over the topologies in the analysis
            origdata = eval(txname.txnameData.origdata) # unmodified mass + luminosity information
            if not txname.txName in MassiveDictionary: MassiveDictionary[txname.txName]=set() # Create sets [of tuples] within the Dictionary 'MassiveDictionary' for each topology (if not present). These sets contain ordered unique elements.
            ObtainMassTuples(txname)

print(MassiveDictionary.keys())

# Save the dictionary to an external file.
with open( "ATLAS-SUSY-2013-21Dictionary.pcl", "wb" ) as WhatAPickle:
    pickle.dump( MassiveDictionary, WhatAPickle)

exit()
# #-------%-------#
    #(Section 3): Query hull and fill randomly with data points for future event generation.
# #-------%-------#

# Having generated the Convex Hull dictionary in section #2, we can simply generate the data once (run section #2) and now just read in the data from the pickle file.

with open( "8TeVDictionary.pcl", "rb" ) as TheSamePickle:
    MassiveDictionary = pickle.load(TheSamePickle)

Hulls={}

# This function (ConvexHullPlot2d_Query) is to aid visual understanding of the code, and can be commented.
#-------#
def ConvexHullPlot2d_Query (TxKey, SortedMasses, p):
    """2D plot of the convex hull for a topology involving 2 body decays. SortedMasses are those that lie in the convex hull; Hulls[TxKey] is the hull for a certain topology; p is a query point. A boundary is drawn between the outermost points of the convex hull, and the individual points plotted on the graph. """
    if(len(SortedMasses[0])==2):
        plt.plot(SortedMasses[:,0], SortedMasses[:,1], 'o',markersize=4.5)
        for simplex in Hulls[TxKey].simplices:
            plt.plot(SortedMasses[simplex, 0], SortedMasses[simplex, 1], 'k-',)
            plt.title(TxKey)
            plt.plot(p[:,0], p[:,1], 's',markersize=4.5)
        plt.savefig('convex_hull_plots/'+str(TxKey)+'.png')
        plt.close()
#-------#

#Mapping: This should be edited to check a large number of points for each topology.
def QueryHull(OldHull, MassPoints, p):
    """Function to query whether a new mass-point coordinate, p, lies within the convex hull of the given data, OldHull. """
    NewPoints = np.append(MassPoints, p, axis=0)
    NewHull = ConvexHull(NewPoints,qhull_options='QJ')
    if list(OldHull.vertices) == list(NewHull.vertices):
        return True
    else:
        return False


for TxKey, MassValues in MassiveDictionary.items():
    SortedMasses = np.array(sorted(MassValues)) # Mass order the tuples
    print(TxKey)
    #-----------------------#
    

    #These next lines generate one point to check in each topology. We want to edit here to generate an array of data points which we query in the convex hull, Hulls[TxKey], with the good ones (QueryHull->True) being saved for data point generation.

    Hulls[TxKey] = ConvexHull(points=SortedMasses, qhull_options='QJ') #this is the original/old hull.
    if(len(SortedMasses[0])==2): # Check whether the hull is 2- or 3- dimensional and generate one random query point within the range below.
        p = np.random.rand(1, 2)*400 # 1 random 2D query point
    else: p = np.random.rand(1, 3)*400 # 1 random 3D query point


  
    #-----------------------#

    # Proof of concept: can be commented.
    # Write out to terminal whether the point p is within the hull, and if the topology is 2D, then to plot the old hull with the new query point.
    #print("Is point p", p, " in hull? ", QueryHull(Hulls[TxKey],SortedMasses,p))
   
   
   
   
   
   
   
   
   
    if(len(SortedMasses[0])==2):
        ConvexHullPlot2d_Query(TxKey, SortedMasses, p)
    else:
        

        plt.plot(SortedMasses[:,0], SortedMasses[:,1], 'o',markersize=4.5)
        for simplex in Hulls[TxKey].simplices:
            plt.plot(SortedMasses[simplex, 0], SortedMasses[simplex, 1],'k-')
        plt.title(TxKey)
        #plt.colorbar()
           # plt.plot(p[:,0], p[:,1], 's',markersize=4.5)
        plt.savefig('convex_hull_plots/'+str(TxKey)+'_01.png')
        plt.close()
        
        plt.plot(SortedMasses[:,0], SortedMasses[:,2], 'o',markersize=4.5)
        for simplex in Hulls[TxKey].simplices:
            plt.plot(SortedMasses[simplex, 0], SortedMasses[simplex, 2],'k-')
        plt.title(TxKey)
        #plt.colorbar()
           # plt.plot(p[:,0], p[:,1], 's',markersize=4.5)
        plt.savefig('convex_hull_plots/'+str(TxKey)+'_02.png')
        plt.close()
        
        plt.plot(SortedMasses[:,1], SortedMasses[:,2], 'o',markersize=4.5)
        for simplex in Hulls[TxKey].simplices:
            plt.plot(SortedMasses[simplex, 1], SortedMasses[simplex, 2],'k-')
        plt.title(TxKey)
        #plt.colorbar()
           # plt.plot(p[:,0], p[:,1], 's',markersize=4.5)
        plt.savefig('convex_hull_plots/'+str(TxKey)+'_12.png')
        plt.close()

    #-----------------------#
