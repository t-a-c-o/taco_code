#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 31 18:18:08 2019

@author: gonzales
"""

import pandas as pd
import os


        
        
def copy_stuff(topo,scan_name):
    os.system('mkdir '+scan_name+topo)
    os.system('mkdir '+scan_name+topo+'/correlation_output')
    os.system('cp -r ../generating_slhas/slhas_ATLAS-SUSY-2013-21/'+topo+' '+scan_name+topo+'/slhas')
    os.system('cp mother_proc.txt '+' '+scan_name+topo+'/proc.txt')
    os.system('cp mother_launch.txt '+' '+scan_name+topo+'/launch.txt')
    
    return
    
def edit_stuff(topo,scan_name,mad_name):

    old_launch_card=open(scan_name+topo+'/launch.txt').read()
    old_proc_card=open(scan_name+topo+'/proc.txt').read()
    
    new_launch_data=old_launch_card.replace('SCAN_NAME/TOPO',mad_name+'/'+topo)
   # new_launch_data=new_launch_data.replace('set nevents=10','set nevents=100')
    
    new_proc_data=old_proc_card.replace('SCAN_NAME/TOPO',mad_name+'/'+topo)
    
    new_launch_card=open(scan_name+topo+'/launch.txt','w')
    new_proc_card=open(scan_name+topo+'/proc.txt','w')
    
    new_launch_card.write(new_launch_data)
    new_proc_card.write(new_proc_data)

    
    new_launch_card.close()
    new_proc_card.close()
    
    return
    
    
    
    
    
    
#topologies=['T1', 'T2', 'T5WW', 'T5WWoff', 'T5WZh', 'T5ZZ', 'T6WW', 'T6WWoff', 'T6WZh', 'T3GQ', 'T5GQ', 'TGQ', 'TStauStau', 'TChiWZ', 'TChiWZoff', 'T6bbHH', 'TChiWW', 'TChipChimSlepSlep', 'TSlepSlep', 'TChiWH', 'T1bbbb', 'T1tttt', 'T1ttttoff', 'T2bb', 'T2tt', 'T2ttoff', 'TChiChipmSlepL', 'TChiChipmSlepStau', 'TChiChipmStauStau', 'T6bbWW', 'T5tctc', 'T5', 'T1btbt', 'T6bbWWoff', 'TChipChimSlepSnu', 'TChiWWoff', 'T2cc', 'T2bbWW', 'T2bbWWoff', 'T2bt', 'T5bbbb', 'T5tttt', 'TChiZZ']

#topologies=['T1', 'T2', 'T5WW', 'T5WWoff', 'T5WZh', 'T5ZZ', 'T6WW', 'T6WWoff', 'T6WZh', 'T3GQ', 'T5GQ', 'TGQ', 'TStauStau', 'TChiWZ', 'TChiWZoff', 'T6bbHH', 'TChiWW', 'TChipChimSlepSlep', 'TSlepSlep', 'TChiWH', 'T1bbbb', 'T1tttt', 'T1ttttoff', 'T2bb', 'T2tt', 'T2ttoff', 'TChiChipmSlepL', 'TChiChipmSlepStau', 'TChiChipmStauStau', 'T6bbWW']
topologies=['T2cc', 'T2bb', 'T2bbWW', 'T2bbWWoff']

scan_name='TACO_ATLAS-SUSY-2013-21/'
mad_name='TACO_ATLAS-SUSY-2013-21'
for topo in topologies:
    copy_stuff(topo,scan_name)
    edit_stuff(topo,scan_name,mad_name)
    
